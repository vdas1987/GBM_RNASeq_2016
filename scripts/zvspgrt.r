library("DESeq2")
#countTable = read.table("/scratch/GT/vdas/pietro/RNA-Seq/IPSCvsTumor/salmon.out/35M_NIPSCvsTUM/salmon.genes.counts", header = TRUE, row.names=1)
#countTable_round<-round(countTable)
#keep <- rowSums(countTable_round)>0
#countTable_round <- countTable_round[keep,]
#dim(countTable_round)
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
gbm.counts.round<-round(gbm.counts)
gbm.z.pgrt<-gbm.counts.round[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4")]
dim(gbm.z.pgrt)
keep <- rowSums(gbm.z.pgrt)>0
gbm.z.pgrt <- gbm.z.pgrt[keep,] # 21162
batch.z.pgrt<-batch[colnames(gbm.z.pgrt),]
write.table(batch.z.pgrt,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.z.pgrt.txt",sep="\t",row.names=T)
batch.z.pgrt <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.z.pgrt.txt",header=T,row.names=1)
dge <- DGEList(counts= gbm.z.pgrt)
dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)
### plotting to see if there is a batch effect or not
plotMDS(logCPM,dim.plot=c(1,2),col=as.numeric(batch.z.pgrt$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
pca= prcomp( logCPM)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.z.pgrt$batch))
text(pca$rotation[,1],pca$rotation[,2], row.names(pca$rotation), cex=0.4, pos=3)
mcor=cor(logCPM,method="spearman")
### plotting the above non correct logCPM correlated data
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =20, cellheight =20,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean")
pca= prcomp( logCPM, center=T)

### trying to see and remove to see if there is a batch correction needed
mm <-model.matrix(~batch.z.pgrt$cellTYPE)
logCPM.corrected.combat <- sva::ComBat(logCPM, batch=as.factor(batch.z.pgrt$batch), mod = mm)
plotMDS(logCPM.corrected.combat,dim.plot=c(1,2),col=as.numeric(batch.z.pgrt$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
mcor=cor(logCPM.corrected.combat,method="pearson")
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =20, cellheight =20,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean")
pca= prcomp( logCPM.corrected.combat)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.z.pgrt$batch))
text(pca$rotation[,1],pca$rotation[,2], row.names(pca$rotation), cex=0.4, pos=3)

### There is a mild batch effect which is somewhat removed with combat 





##svaseq does not work only combat with known batch effects work to an extent

colData <- data.frame(row.names = colnames(gbm.z.pgrt),condition = ~batch.z.pgrt$batch+batch.z.pgrt$cellTYPE)
dat <- counts(dds, normalized=TRUE)
design <- model.matrix(~ batch.z.pgrt$batch+ batch.z.pgrt $cellTYPE)
design0 <- model.matrix(~ batch.z.pgrt $batch)
n.sv <- num.sv(logCPM,design,method="leek")
sv <- sva(as.matrix(logCPM),design,design0,n.sv)
head(countTable)

#### So running DESEQ2 without batch correction just using the condition and finding the DEGs ( this is still ok but not entirely accurate)


colData <- data.frame(row.names = colnames(gbm.z.pgrt),condition = factor(c(batch.z.pgrt$cellTYPE)))
colData
#mm <-model.matrix(~batch.z.pgrt$batch+batch.z.pgrt$cellTYPE)
dds <- DESeqDataSetFromMatrix(countData = gbm.z.pgrt,colData = colData,design = ~ condition)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh<-subset(resOrdered,padj<0.05) #545 degs #date 23-08-2016: 512 degs
## Volcano plot
tmz_clean<-as.matrix(thresh[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(thresh)), ]) ## removing the tmz genes
degs.z.pgrt_clean <-data.frame(tmz_clean) #511 degs

with(resOrdered, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1, green if both)
with(subset(resOrdered, padj <0.05 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(resOrdered, abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="orange"))
with(subset(degs.z.pgrt_clean, padj <0.05 & abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="grey60")) ## now take these genes and plot heatmap
p1<-subset(degs.z.pgrt_clean, padj <0.05 & abs(log2FoldChange)>1) ## heatmap of these genes 494 ## now 463 on 23rd aug 2016
write.table(p1,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/deseq2.degs.zvspgrt_no_batch_correction.txt",sep="\t",row.names=T)
##heatmap
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]
gbm.z.pgrt.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4")]
#rgheatmap(gbm.z.pgrt.tpm[rownames(p1),], scale="row",annotation=batch.z.pgrt, fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/hm_zvspgrt_degs_clean_without_batch_correction_deseq2_out.png")

##on 23rd august
rgheatmap(gbm.z.pgrt.tpm[rownames(p1),], scale="row",annotation=batch.z.pgrt, fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/hm_zvspgrt_degs_clean_without_batch_correction_deseq2_out.23aug.png")

#### now trying to run DESeq2 with combat correction to see number of genes as degs (have to find a way how to use batch effect here) as per blogs there are negative values that happen due to batch correction with combat so changed them to 0. This might not be a very correct form since batch correction for 13 samples gives a lot of negative values and this combat values are then used for differential expression after changing the negative values to 0 , this might not be the correct form, so it is better to add the batch and condition in the design matrix for a multi factorial design. This model the batch effect in the regression step, which means that the raw data are not modified (so the batch effect is not removed), but instead the regression will estimate the size of the batch effect and subtract it out when performing all other tests. In addition, the model's residual degrees of freedom will be reduced appropriately to reflect the fact that some degrees of freedom were "spent" modelling the batch effects. This is the preferred approach for any method that is capable of using it (this includes DESeq2).
colData <- data.frame(row.names = colnames(gbm.z.pgrt),condition = factor(c(batch.z.pgrt$cellTYPE)))
colData
mm <-model.matrix(~batch.z.pgrt$batch+batch.z.pgrt$cellTYPE)
combat_dat <- ComBat(dat=gbm.z.pgrt, batch=batch.z.pgrt$batch, mod=mm[,1],par.prior=TRUE, prior.plots=TRUE)
combat_dat[combat_dat <0] <- 0
combat_dat.round<-round(combat_dat)

dds1 <- DESeqDataSetFromMatrix(countData = combat_dat.round,colData = colData,design = ~ condition)
dds1
ddds1 <- DESeq(dds1)
res1 <- results(ddds1)
res1
resOrdered1<-res1[order(res1$padj),]
head(resOrdered1)
thresh1<-subset(resOrdered1,padj<0.05) # 341

tmz_clean1<-as.matrix(thresh1[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(thresh1)), ]) ## removing the tmz genes
degs.z.pgrt_clean1 <-data.frame(tmz_clean1) #(340 genes)
write.table(degs.z.pgrt_clean1,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/deseq2.degs.zvspgrt_with_batch_correction.txt",sep="\t",row.names=T)
##volcano plot
## with higher FC values
with(resOrdered1, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1.5, green if both)
with(subset(degs.z.pgrt_clean1, padj <0.05 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(degs.z.pgrt_clean1, abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="orange")) #gives 160 genes so inflate the FC value
with(subset(degs.z.pgrt_clean1, padj <0.05 & abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="grey60"))
p.corrected<-subset(degs.z.pgrt_clean1, padj <0.05 & abs(log2FoldChange)>1) ## 160
### heatmap of the 160 genes
rgheatmap(gbm.z.pgrt.tpm[rownames(p.corrected),], scale="row",annotation=batch.z.pgrt, fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/hm_zvspgrt_degs_clean_with_batch_correction_deseq2_out.png")
### The above usage of removing batch effect with combat might not be the correct way to proceed as it is not clear whether om tranformaing the log combat is a correct way and then feeding to DESEQ2 so it is better to use the multifactorial of implementing the batch inside the design in order to appropriately estimate the size of batch effect and then substract it while performing tests. Since combat prefers log transformed values and then corrects for known batches and this gives negative values at times if there is tailing data on either side of distribution. So preferrably it is not a correct method as deseq expects to get raw counts with rounded off values, so better is to do multifacotorial where batch will be used in the design which will be regressed and substracted. 

### Multifactorial design with DESEQ2 for  z v pgrt with individuals

colData <- data.frame(row.names = colnames(gbm.z.pgrt),batch=factor(c(batch.z.pgrt$batch)),condition = factor(c(batch.z.pgrt$cellTYPE)))
colData
mm<- model.matrix(~batch+condition, colData)
dds <- DESeqDataSetFromMatrix(countData = gbm.z.pgrt,colData = colData,design = ~ batch + condition)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh.multi.factor<-subset(resOrdered,padj<0.05)# 629 genes #561 degs aug 23
degs.multi.factor<-subset(resOrdered,padj<0.05 & abs(log2FoldChange)>1) # 426 #373 on aug 23

## remove tmz genes
tmz_clean.degs.mf<-as.matrix(degs.multi.factor[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(degs.multi.factor)), ]) ## removing the tmz genes
degs.z.pgrt_clean.mf <-data.frame(tmz_clean.degs.mf) # 426 genes # 373 degs

write.table(degs.z.pgrt_clean.mf,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/deseq2.degs.zvspgrt_multifactorial.txt",sep="\t",row.names=T)
#### volcano plot
with(resOrdered, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1, green if both)
with(subset(resOrdered, padj <0.05 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(resOrdered, abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="orange")) #
with(subset(degs.z.pgrt_clean.mf, padj <0.05 & abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="grey60"))
###aug 23
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]
gbm.z.pgrt.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4")]
#ltpm<-log2(gbm.z.pgrt.tpm+1) not needed here
## heatmap
#rgheatmap(gbm.z.pgrt.tpm[rownames(degs.z.pgrt_clean.mf),], scale="row",annotation=batch.z.pgrt, fontsize=5.5,clustering_distance_cols="maximum",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/hm_zvspgrt_degs_multifactor_deseq2_out.png")
## aug 23
rgheatmap(gbm.z.pgrt.tpm[rownames(degs.z.pgrt_clean.mf),], scale="row",annotation=batch.z.pgrt, fontsize=5.5,clustering_distance_cols="maximum",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/hm_zvspgrt_degs_multifactor_deseq2_out.aug23.png")


WNK4|MDGA2|POSTN|ICAM1|DMTN|CHGB|ANO1|HSPB3|DCLK1|SPOCK2|TMEM63C|SULF1|GCHFR|JAG2|CLDN4|DDO|LOC284648|PLD5|ICAM5|ETNK2|CLEC18A|ANKRD24|LY96|SCAMP5|C10orf35|TM6SF2|ST3GAL6|CELSR3|MRPL42P5|MMP17|BNC2|APOBEC3C|HNRNPA1P10|BICC1|NQO1|TEX41|PDGFRA|PALMD|SLC7A2|ADAMTS15|JUP|DNAAF3|CLMP|PPARGC1B|SNORD17|ECSCR|DRD1|TDRD1

ex.degs.PGRT.PDGRT.tpm<-as.matrix(gbm.pgrt.pdgrt.tpm[grepl("WNK4|MDGA2|POSTN|ICAM1|DMTN|CHGB|ANO1|HSPB3|DCLK1|SPOCK2|TMEM63C|SULF1|GCHFR|JAG2|CLDN4|DDO|LOC284648|PLD5|ICAM5|ETNK2|CLEC18A|ANKRD24|LY96|SCAMP5|C10orf35|TM6SF2|ST3GAL6|CELSR3|MRPL42P5|MMP17|BNC2|APOBEC3C|HNRNPA1P10|BICC1|NQO1|TEX41|PDGFRA|PALMD|SLC7A2|ADAMTS15|JUP|DNAAF3|CLMP|PPARGC1B|SNORD17|ECSCR|DRD1|TDRD1", rownames(gbm.pgrt.pdgrt.tpm)), ])

keep <- rowSums(ltpm.ex.degs.pgrt.pdgrt)>0
ltpm.ex.degs.pgrt.pdgrt.clean <- ltpm.ex.degs.pgrt.pdgrt[keep,]

### removed the NA value from 373 degs to give 372 and plotted only 372 degs with mf deseq2 on ltpm with
pheatmap(clean.degs.z.pgrt, scale="row",annotation=batch.z.pgrt[,1:5], fontsize=5.5,clustering_distance_cols="canberra",show_rownames = F,show_colnames=F,color=col,treeheight_col = 50)


pheatmap(gbm.z.pgrt.tpm[rownames(df3),], scale="row",annotation=batch.z.pgrt[,3:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,color = col,show_colnames = T)


###overlaying of foldchanges
resOrdered.z.pgrt<-resOrdered

degs.18.up.exclu.PGRT.PDGRT.Z<-c("BNC2","APOBEC3C","HNRNPA1P10","BICC1","NQO1","TEX41","PDGFRA","PALMD","SLC7A2","ADAMTS15","JUP","DNAAF3","CLMP","PPARGC1B","SNORD17","ECSCR","DRD1","TDRD1")

degs.30.up.exclu.PGRT.PDGRT.Z<-c("WNK4","MDGA2","POSTN","ICAM1","DMTN","CHGB","ANO1","HSPB3","DCLK1","SPOCK2","TMEM63C","SULF1","GCHFR","JAG2","CLDN4","DDO","LOC284648","PLD5","ICAM5","ETNK2","CLEC18A","ANKRD24","LY96","SCAMP5","C10orf35","TM6SF2","ST3GAL6","CELSR3","MRPL42P5","MMP17")

### overalying of Log2FC of the 18 degs

df1<-resOrdered.z.pgrt[(rownames(resOrdered.z.pgrt) %in% degs.18.up.exclu.PGRT.PDGRT.Z),]
df2<-resOrdered.z.pdgrt[(rownames(resOrdered.z.pdgrt) %in% degs.18.up.exclu.PGRT.PDGRT.Z),]
df3<-resOrdered.pgrt.pdgrt[(rownames(resOrdered.pgrt.pdgrt) %in% degs.18.up.exclu.PGRT.PDGRT.Z),]


d1 <- density(df1$log2FoldChange) # 18 in ZvsPGRT log2FC
d2 <- density(df2$log2FoldChange) # same 18 in ZvsPDGRT log2FC
d3 <- density(df3$log2FoldChange) # same 18 in PGRT vs PDGRT log2FC

plot(d1, main="Density plot of FC for 18 DEGs up only in PGRT",col="red",xlim=c(-1,2),ylim=c(0,4))
lines (d2,col="blue")
lines (d3,col="dark green")

### overlaying of log2FC of the 30 degs that are exclusively down in PGRT and check their log2FC density across ZvsPGRT,ZvsPDGRT and PGRTvsPDGRT

df1<-resOrdered.z.pgrt[(rownames(resOrdered.z.pgrt) %in% degs.30.up.exclu.PGRT.PDGRT.Z),]
df2<-resOrdered.z.pdgrt[(rownames(resOrdered.z.pdgrt) %in% degs.30.up.exclu.PGRT.PDGRT.Z),]
df3<-resOrdered.pgrt.pdgrt[(rownames(resOrdered.pgrt.pdgrt) %in% degs.30.up.exclu.PGRT.PDGRT.Z),]


d1 <- density(df1$log2FoldChange) # 30 in ZvsPGRT log2FC
d2 <- density(df2$log2FoldChange) # same 30 in ZvsPDGRT log2FC
d3 <- density(df3$log2FoldChange) # same 30 in PGRT vs PDGRT log2FC

plot(d1, main="Density plot of FC for 30 DEGs down only in PGRT",col="red",xlim=c(-4,2),ylim=c(0,6))
lines (d2,col="blue")
lines (d3,col="dark green")

### plots the 48 degs exlusive in Z vs PGRT once discounted from Z vs PDGRT

degs.48..exclu.Z.PGRT.from.PDGRT<-c("BNC2","APOBEC3C","HNRNPA1P10","BICC1","NQO1","TEX41","PDGFRA","PALMD","SLC7A2","ADAMTS15","JUP","DNAAF3","CLMP","PPARGC1B","SNORD17","ECSCR","DRD1","TDRD1","WNK4","MDGA2","POSTN","ICAM1","DMTN","CHGB","ANO1","HSPB3","DCLK1","SPOCK2","TMEM63C","SULF1","GCHFR","JAG2","CLDN4","DDO","LOC284648","PLD5","ICAM5","ETNK2","CLEC18A","ANKRD24","LY96","SCAMP5","C10orf35","TM6SF2","ST3GAL6","CELSR3","MRPL42P5","MMP17")

df.degs.48.exclu.Z.PGRT.from.PDGRT<-gbm.z.pgrt.tpm[(rownames(gbm.z.pgrt.tpm) %in% degs.48..exclu.Z.PGRT.from.PDGRT),]

batch.z.pgrt<-batch.gbm.new[colnames(gbm.z.pgrt.tpm),]

pheatmap(log2(df.degs.48.exclu.Z.PGRT.from.PDGRT+1), scale="row",annotation=batch.z.pgrt[,2:4], fontsize=8,clustering_distance_cols="binary",show_rownames = F,treeheight_col=40,color = col,show_colnames = F,clustering_method = "complete",clustering_distance_rows = "correlation",cellwidth =15)


### preparing the 48 DEGs exclusively changing between ZvsPGRT for motif analysis by pscan
tx2gene <- read.table("/Users/vdas/Documents/Test_lab_projects/Pietro_Ova_data_092015/RNA-Seq/output/hg19/transcript2gene.txt",header=T)

df.degs.48.exclu.Z.PGRT.from.PDGRT
df3.degs.z.pdgrt.total
write.table(df.degs.48.exclu.Z.PGRT.from.PDGRT,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/48.degs.exclus.z.pgrt.txt",sep="\t",row.names=T)


df1<-read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/48.degs.exclus.z.pgrt.txt",header=T)

refseq.48<-merge(tx2gene,df1,by="ID") ## pscan on this degs revealed 21 TFs upstream with pvalue < 0.05 which did not have any overlap with the 48 degs, try to check the expression of these genes in pgrt vs pdgrt and see how they behave


write.table(refseq.48,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/48.degs.refseq.exclus.z.pgrt.txt",sep="\t",row.names=T)


