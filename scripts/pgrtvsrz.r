###DEGs with PGRT vs RZ

batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
gbm.counts.round<-round(gbm.counts)

keep <- rowSums(gbm.counts.round)>0
gbm.counts.round <- gbm.counts.round[keep,]
dim(gbm.counts.round) # 22397

gbm.pgrt.rz<-gbm.counts.round[,c("udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.91rz","udine.118rz","udine.132rz1","udine.132rz2")]

keep <- rowSums(gbm.pgrt.rz)>0
gbm.pgrt.rz <- gbm.pgrt.rz[keep,] #21521

batch.pgrt.rz<-batch[colnames(gbm.pgrt.rz),] 

dge <- DGEList(counts= gbm.pgrt.rz)
dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)

pca= prcomp( logCPM)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.pgrt.rz$batch))
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.pgrt.rz$cellTYPE))

text(pca$rotation[,1],pca$rotation[,2], row.names(pca$rotation), cex=0.4, pos=3)### Running DESeq2 with MF design

mcor=cor(logCPM,method="spearman")

pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =20, cellheight =20,cluster_cols=T,clustering_distance_rows="correlation",clustering_distance_cols="correlation",annotation = batch.pgrt.rz[,1:5])

#batch corrected data

mm <-model.matrix(~batch.pgrt.rz$cellTYPE)
logCPM.corrected.combat <- sva::ComBat(logCPM, batch=as.factor(batch.pgrt.rz$batch), mod = mm[,1])

mcor=cor(logCPM.corrected.combat,method="spearman")

pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =20, cellheight =20,cluster_cols=T,clustering_distance_rows="correlation",clustering_distance_cols="correlation",annotation = batch.pgrt.rz[,1:5])

####

colData <- batch.pgrt.rz[,c("patient","cellTYPE")]
colData
dds <- DESeqDataSetFromMatrix(countData = gbm.pgrt.rz,colData = colData,design = ~ patient + cellTYPE)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh<-subset(resOrdered,padj<0.05) ## 6294


tmz_clean<-as.matrix(thresh[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(thresh)), ]) ## removing the tmz genes
degs.pgrt.rz_clean <-data.frame(tmz_clean) #6283

with(resOrdered, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1, green if both)
with(subset(resOrdered, padj <0.01 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(resOrdered, abs(log2FoldChange)>1.5), points(log2FoldChange, -log10(padj), pch=20, col="orange"))
with(subset(degs.pdgrt.rz_clean, padj <0.01 & abs(log2FoldChange)>1.5), points(log2FoldChange, -log10(padj), pch=20, col="grey60")) ## now take these genes and plot heatmap
degs.pgrt.rz.flt<-subset(degs.pgrt.rz_clean, padj <0.01 & abs(log2FoldChange)>1.5) ## heatmap of these genes 3446
write.table(degs.pgrt.rz.flt,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.degs.pgrt.rz.mf.txt",sep="\t",row.names=T)
##heatmap
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]
gbm.pgrt.rz.tpm<-gbm_tpm[,c("udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.91rz","udine.118rz","udine.132rz1","udine.132rz2")]
#heatmap
ltpm.pgrt.rz<-log2(gbm.pgrt.rz.tpm+1)
rgheatmap(ltpm.pgrt.rz[rownames(degs.pgrt.rz.flt),],fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,annotation = batch.pgrt.rz[,1:5],scale="row")

###Trying find heatmaps of TFs as DEGs , we found 261 TFs as DEGs


TFs.DEGs.pgrt.rz<-c("NR2F2","SOX2","STAT6","FOXS1","EBF1","HOXB2","TBX18","FOXC2","HOXB3","SNAI2","EN1","SOX9","HEYL","SOX21","LHX2","ID1","HEY1","SIX2","NKX2-2","POU3F2","SOX11","NPAS3","POU2F2","ID2","NFIB","HIVEP3","AHRR","HOXC10","NFIX","MSC","SOX3","FOXL1","INSM1","NKX2-5","HOPX","ZIC2","KDM5D","STAT4","XBP1","PROX1","SHOX2","HOXD11","DMRT2","TOX3","TBX15","NR3C2","HOXA7","HOXB4","RUNX3","BHLHE41","ZNF536","EN2","TOX","HMX1","SALL3","PLAGL1","CDX1","TP73","HAND2","AHR","STAT1","ZNF521","HOXC6","EBF2","HMGA2","ZFHX4","GLIS3","TBX2","NFATC4","NKX6-1","ZNF436","HOXD10","HIVEP2","CREB3L1","BACH1","CREB3L2","SP100","SOX6","FOXG1","KLF5","ARID3A","PAX2","ATF4","ARNT2","MEIS1","POU3F3","FOSB","FOXF1","HOXA10","PAX3","MEF2D","ID4","NR4A1","HOXB7","HOXC4","NR4A3","OLIG2","ZIC4","L3MBTL4","MAFF","HOXD13","TBX5","LEF1","ZFY","TFAP2C","SP140L","HOXC8","OLIG1","ISL2","GSC","ETS1","HNF4G","SMAD9","MAFK","NFE2L3","NR2E1","PITX1","OTP","HOXB5","YY2","SALL1","HOXA3","ZIC1","ZNF560","MSX2","ALX1","SMAD1","SOX4","PAX6","EGR2","HOXC9","CREB5","TRPS1","SMAD3","VTN","HOXC13","HLF","RFX3","MLXIP","SOX5","ATOH7","IRX1","SMAD6","GLIS1","SOX8","OSR1","MESP1","HIVEP1","KLF4","ZNF680","SALL2","DBP","ZNF467","BACH2","ZFP37","E2F5","MYC","EBF4","IRX3","SOX1","IRF1","HLX","ELF4","SOX10","ATF3","ZNF367","HOXB9","FOXO1","HOXD3","IRF5","ZNF48","ZNF711","HOXA5","HEY2","NFIL3","TFAP2A","ZNF92","ZNF793","CEBPA","AFF3","ESRRG","NKX3-2","FOXL2","SATB2","ZNF43","SP110","GLI2","ZNF649","TP63","KLF11","ZNF658","NFATC1","SMAD7","SREBF1","TSHZ1","SNAI1","MECOM","FEZF1","ZNF254","CSDC2","MYBL1","PPARG","HOXA4","FOXA3","FOXF2","ZNF334","DMRTA2","GLI1","DLX1","MAFB","SATB1","OTX1","ZNF708","ZNF699","ZNF606","PGR","ZNF253","KLF2","HOXD4","FOXD3","ZBTB7C","ZBTB42","ZIC5","ETV3","PRRX2","ZBTB46","ZNF682","ARX","RFX4","TLX2","POU6F1","THRB","ZNF681","ONECUT2","ZNF107","PITX2","VAX2","MYCN","ZNF138","FOXJ1","KLF15","HOXA1","HIC1","ZNF678","NR4A2","ZFPM2","POU3F1","ZNF100","GATA6","GFI1B","LYL1","NR6A1","LBX2","HHEX","SP6","VDR","FOXE1","FOS","DLX5","FOXP2","SALL4") 


TFs.DEGs.pgrt.rz.df<-degs.pgrt.rz.flt[(rownames(degs.pgrt.rz.flt) %in% TFs.DEGs.pgrt.rz),]

write.table(TFs.DEGs.pgrt.rz.df,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.TF.DEGs.PGRTvsRZ.mf.txt",sep="\t",row.names=T)

col=colorpanel(120, "dark blue", "white","dark red")

pheatmap(ltpm.pgrt.rz[rownames(TFs.DEGs.pgrt.rz.df),], scale="row",annotation=batch.pgrt.rz[,1:5],clustering_distance_cols="correlation",show_rownames = F,color = col,show_colnames = F)

### List of oncogenes that are changing in between PGRT vs RZ
TF.Onco.DEGs.PGRT.RZ<-c("AFF3","CEBPA","CREB3L1","CREB3L2","EBF1","ELF4","ETS1","FOSB","FOS","FOXL2","FOXO1","HEY1","HLF","HMGA2","HOXC13","HOXD11","HOXD13","LYL1","MAFB","MAFF","MAFK","MECOM","MYBL1","MYCN","MYC","NFIB","NR4A3","OLIG2","PAX3","PPARG","SOX2","ZNF521")
TF.Onco.DEGs.PGRT.RZ.df<-degs.pgrt.rz.flt[(rownames(degs.pgrt.rz.flt) %in% TF.Onco.DEGs.PGRT.RZ),]

col=colorpanel(120, "dark blue", "black","yellow")

pheatmap(ltpm.pgrt.rz[rownames(TF.Onco.DEGs.PGRT.RZ.df),], scale="row",annotation=batch[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)
### refseq list from DEGs

 tx2gene <- read.table("/Users/vdas/Documents/Test_lab_projects/Pietro_Ova_data_092015/RNA-Seq/output/hg19/transcript2gene.txt",header=T)
DEGs.PGRT.RZ.test <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/DEGs.PGRTvsRZ.txt",header=T)

ls.test<-list[!duplicated(list$ID),]
write.table(ls.test,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/DEGs.PGRTvsRZ.refseq.txt",sep="\t",row.names=T)

#### Trying to see which DEGs if they are in one of the signatures
#write.table(degs.PT.ST.final,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.degs.PTvsST.mf.txt",sep="\t",row.names=T)
degs.PGRT.RZ.df <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.degs.pgrt.rz.mf.txt",header=T,row.names=1)

gbm.pgrt.rz<-ltpm.gbm.all[,c("udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.91rz","udine.118rz","udine.132rz1","udine.132rz2")]

ltpm.gbm.pgrt.rz<-log2(gbm.pgrt.rz+1)
batch.pgrt.rz<-batch.gbm.new[colnames(ltpm.gbm.pgrt.rz),] 

degs.vk.cl.pgrt.rz.df<-degs.PGRT.RZ.df[(rownames(degs.PGRT.RZ.df) %in% vk.cl),] # 60 degs  p < 5.174e-11 
degs.vk.mes.pgrt.rz.df<-degs.PGRT.RZ.df[(rownames(degs.PGRT.RZ.df) %in% vk.mes),] # 71 degs p < 5.081e-10 
degs.vk.PN.pgrt.rz.df<-degs.PGRT.RZ.df[(rownames(degs.PGRT.RZ.df) %in% vk.PN),] # 72 degs p < 1.149e-15 
degs.vk.NL.pgrt.rz.df<-degs.PGRT.RZ.df[(rownames(degs.PGRT.RZ.df) %in% vk.NL),] #  29 degs p < 0.032 


### Two ord plot for the enriched signatures another way of representing the verhaak signautures in PT vs REC
library(plotrix)
a=c(71,60,72,29)
b=c(9.29,10.29,14.94,1.49)
genes=c("MES","CL","PN","NL")
genesnum<-1:4
twoord.plot(genesnum,a, genesnum,b,type=c("bar","l"),ylab="Number of genes enriched per signature",rylab="Log10(P.Value)",xlab="Verhaak Signatures",lcol=17,rcol=3,do.first="plot_bg()", xticklab= genes,lpch=1,halfwidth=0.1,axislab.cex=1)

keep <- rowSums(ltpm.gbm.pgrt.rz)>0
ltpm.gbm.pgrt.rz.exp <- ltpm.gbm.pgrt.rz[keep,]
col=colorpanel(100, "black","white","dark red")

## combined heatmap of all signature in DEGs PT vs ST with annotation

#degs.verhaak<-c("DLC1","ERCC2","JAG1","GAS1","GLI2","ZFHX4","TBX2","EGFR","MCC","SLC4A4","SHOX2","RGS6","PMP22","CDH2","EYA2","MEIS1","TES","CD4","COL1A1","THBS1","THBD","COL1A2","MAPK13","C5AR1","DAB2","MGAT1","ARHGAP29","MS4A4A","PROCR","STAT6","TGFBI","SEC24D","SH2B3","FHOD1","STXBP2","TCIRG1","SERPINE1","SLC16A3","LTBP2","LOX","TRIM38","SP100","LCP2","CDCP1","MYH9","S100A4","ITGA5","DSE","TEC","TNFRSF11A","MAFB","RAB11FIP1","PLAUR","TRPM2","PTGER4","COL5A1","FES","PTPRC","CD14","NOD2","VDR","FHL2","ITGA4","RAC2","MSR1","PLCB4","PCDH11X","MTSS1","KIF21B","CA10","RAB33A","SLC1A1","EPHB1","NRXN1","NR0B1","C1QL1","REEP1","NLGN3","GADD45G","ARRB1","ACSL4","CALM2","SLC30A10","SNCG","PPFIA2","PLCL1","CASQ1","FUT9")

ann_row <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/DEGs.PGRT.RZ.annot.txt",header=T,row.names=1)

#degs.vk.all.sig.df<-degs.PT.ST.df[(rownames(degs.PT.ST.df) %in% degs.verhaak),]

as.matrix(ltpm.gbm.pgrt.rz[grepl("^RAB11FIP$", rownames(ltpm.gbm.pgrt.rz)), ])

pheatmap(ltpm.gbm.pgrt.rz[rownames(ann_row),],annotation_col=batch.pgrt.rz,  annotation_row=ann_row,fontsize=6,show_rownames = F,show_colnames = F,cluster_rows=F,cluster_cols=T,scale="row",color = col,cellwidth = 30,)

##verhaak all
 verhak.PGRTvsRZ<-ltpm.gbm.pgrt.rz.exp[rownames(ann_row),]
> dim(verhak.PGRTvsRZ)
[1] 682  11
> head(verhak.PGRTvsRZ)
      udine.132p1 udine.141p1 udine.183p2 udine.183p3  ieo.127P1  ieo.168P3 ieo.168P4 udine.91rz udine.118rz udine.132rz1
CDK6   1.80718834   1.9657032  1.62333100  2.09002987 2.44654537 2.39005868 2.3575026  2.2748809  1.64389269    2.5789979
ABCD2  0.01908393   0.0198807  0.05267048  0.03478371 0.06776331 0.07483054 0.1224851  0.2957066  0.02989778    0.8750293
GAS1   0.46962149   0.2711053  1.41110296  1.09344289 0.18513295 1.54868232 1.3261944  2.1820500  1.06722885    2.9946926
TGIF2  1.64607229   1.6878627  1.71502134  1.78687549 1.70990763 1.65355463 1.5249895  2.2600446  1.29616950    1.8023440
TBX2   2.91514629   2.8592242  2.99557571  2.87906916 2.58646204 2.87329500 2.8500595  2.9011547  2.13328002    2.3119492
DOCK6  1.98776068   2.0272284  2.11150792  2.07891931 1.65996309 1.60728741 1.5620615  1.9008638  2.13364351    1.6356909
      udine.132rz2
CDK6     2.4693872
ABCD2    0.4039563
GAS1     2.6681933
TGIF2    2.0657028
TBX2     2.1580036
DOCK6    1.8901804
> verhak.PGRTvsRZ.clean<-na.omit(verhak.PGRTvsRZ)
> dim(verhak.PGRTvsRZ.clean)
[1] 630  11
> pheatmap(verhak.PGRTvsRZ.clean,annotation_col=batch.pgrt.rz,  annotation_row=ann_row,fontsize=5.5,show_rownames = F,show_colnames = F,cluster_rows=F,cluster_cols=T,scale="row",color = col,border_color=NA)


write.table(ltpm.gbm.pgrt.rz.exp,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/ltpm.gbm.pgrt.rz.bg.txt",sep="\t",row.names=T)

#### Making GO analysis with GT scripts and also comparing the same with PANTHER

## For up in RC of REC GBM
#MES
up.in.RC.overlap.MES <- c("NRP1","LHFPL2","ADAM12","CTSC","LCP1","WWTR1","DCBLD2","AIM1","CCDC109B","MAFB","CSTA","C5AR1","DSC2","COL8A2","CHI3L1","PDPN")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,up.in.RC.overlap.MES)## no terms
goTreemap(go$BP,removeParents = F)


## PN
up.in.RC.overlap.PN <- c("VAX2","TMCC1","C1orf106","MARCKSL1","FAM110B","ABAT","SATB1","SOX4","MMP16","P2RX7","MLLT11","SCG3","BAI3","PPM1E","FLRT1","MAST1","ZNF804A","EPB41","DNM3","GADD45G","CSPG5","ZNF711","LPHN3","SCN3A","MMP15","FHOD3","CXXC4","PURG","HRASLS","NOL4","FXYD6","SOX10","BCAN","PAK3","REEP1","CRMP1","CRB1","NRXN1","SEZ6L","CNTN1","STMN4","C1orf61","PCDH11X","CKB","EPHB1","GRIA2","MAPT","SOX11","FGF9","SLCO5A1","SRGAP3","NLGN3","DPP6","HOXD3","CA10","C1QL1","SPTBN2","GPM6A","OLIG2","CLGN","TOX3","NCAM1","NKX2-2","TTYH1","SOX2")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,up.in.RC.overlap.PN) ## No terms
goTreemap(go$BP,removeParents = F)

#write.table(go$BP,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/GO.BP.down.in.REC.overlap.MES.txt",sep="\t",row.names=T)

#write.table(go$MF,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/GO.MF.down.in.REC.overlap.MES.txt",sep="\t",row.names=T)

## CL
up.in.RC.overlap.CL <- c("BLM","CDK6","SLC4A4","ZNF606","EXTL3","TGFB3","LMO2","RGS12","SIPA1L1","MAB21L1","FZD7","OSBPL3","MCC","CREB5","SPRY2","ABCD2","PMP22","MYO5C","KCNF1","SEMA6D","HS3ST3B1","FZD3","KLHL4","SMO","NDP","GNG7","CAMK2B","NR2E1","IRS2","CDH2","ZFHX4","RGS6","MEIS1","SEMA6A","SHOX2","WSCD1","SOX9","VAV3","EYA2","GAS1","ITGB8","GPR56","PIPOX","KLHDC8A","NPAS3","GRIK5","CDH4","MLC1")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,up.in.RC.overlap.CL)# no terms
goTreemap(go$BP,removeParents = F)

#####

## For down in RC of REC GBM
#MES
down.in.RC.overlap.MES <- c("CD4","STAT6","COL1A2","THBD","COL1A1","TES","MS4A4A","SLC16A3","THBS1","LCP2","DAB2","TGFBI","ARHGAP29","TCIRG1","CD14","TEC","RAC2","S100A4","PTGER4","FES","MAPK13","MGAT1","LTBP2","TRIM38","PROCR","SERPINE1","CDCP1","SEC24D","STXBP2","P4HA2","DSE","FHOD1","TNFRSF1B","ARPC1B","RAB11FIP1","ITGA4","CASP1","ELF4","CLIC1","IL4R","ASL","SH2B3","PLS3","C1orf54","COL5A1","CTSZ","FHL2","CASP4","SP100","ITGA5","ENG","COPZ2","IFI30","VDR","PHF11")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,down.in.RC.overlap.MES)## 151 below < 0.05 and 55 below < 0.01
goTreemap(go$BP,removeParents = F)
goTreemap(go$MF,removeParents = F)

write.table(go$BP,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/GO.BP.down.in.RC.overlap.MES.txt",sep="\t",row.names=T)

write.table(go$MF,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/GO.MF.down.in.RC.overlap.MES.txt",sep="\t",row.names=T)

## PN
down.in.RC.overlap.PN <- c("NRXN2","PLCB4","RAB33A","MTSS1","SLC1A1","MYO10","AMOTL2")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,down.in.RC.overlap.PN) ## no terms
goTreemap(go$BP,removeParents = F)


## CL
down.in.RC.overlap.CL <- c("DLC1","TBX2","SOCS2","JAG1","EGFR","GLI2","ERCC2","PDGFA","ACSL3","ITGA7","PTPN14","RBCK1")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,down.in.RC.overlap.CL) ## nothing found
goTreemap(go$BP,removeParents = F)

## NL
down.in.RC.overlap.NL <- c("ARRB1","SLC30A10","ENPP2","BASP1","SNCG","ACSL4","FXYD1","CALM2","EDIL3")
bg <- (rownames(ltpm.gbm.pgrt.rz.exp))
go <- go.enrichment(bg,down.in.RC.overlap.NL) ## no terms
goTreemap(go$BP,removeParents = F)