### with limma-voom for primary vs secondary degs finding and correcting for batch of facility wit combat and then plotting the degs for volcano and heatmap(did not use svaseq since there were no other confounding variables , in short only the facilty difference was the driving batch which can either be removed by removebatch() from edgeR or combat for known batches and then and used limma voom for differential expression
library(edgeR)
library(sva)
library(limma)
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
gbm.counts.round<-round(gbm.counts) ## 22507 genes
dge <- DGEList(counts= gbm.counts.round)
design <- model.matrix(~batch$batch+batch$tumortypes)
rownames(design) <- colnames(dge)
dge$samples$lib.size <- colSums(dge$counts)
dge <- calcNormFactors(dge) ## already normalizing here with TMM once and then feed to voom function which usually log tranform the counts this can be fed to combat and then that to lmFIT
## combat-limma voom
v <- voom(dge, design, plot=TRUE) ## this is transformaing the noramlized dge matrix to log2 scale which is fine for combat to be taken as input
combat_edata <- ComBat(dat=v$E, batch=batch$batch, mod=design[,1], par.prior=TRUE, prior.plots=TRUE)
fit <- lmFit(combat_edata, design)
fit <- eBayes(fit)
top <- topTable(fit,coef=3,number=Inf,sort.by="P")
sum(top$adj.P.Val<0.05)
degs<-topTable(fit, adjust.method="BH", coef=3,number=Inf,sort.by="P")
ind<-which(degs$adj.P.Val<0.01 & abs(top$logFC)>1.5) ## taking only degs with logFC 1.5 and adj p value 0.01
degs_0.01_1.5<-degs[ind,]

### remobing the TMZ resistant genes 

tmz_genes_set1<-c("GLUT1","GLUT2","GLUT3","GLUT10","AKR1C1", "AKR1C2","AKR1C3","SCRG1","IGSF4D","SLC2A10","SPP1","SLC2A5","SCG2") #Benjamin le calve eta. al neoplasia ,2010
tmz_genes_set2<-c("MDR1","PTCH1") #Jessian L Munoz et al, Cancer Cell & Microenvironment 2015
tmz_genes_set3<-c("MGMT","APNG","BER","NAMPT","BCRP1","LIF","TNFAIP3","MSH6","MSH2") #Lee SY et all , Genes and Diseases 2016

tmz_clean<-as.matrix(degs_0.01_1.5[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(degs_0.01_1.5)), ]) 
tmz_clean <-data.frame(tmz_clean) ## out of the above only below 3 genes are DEGs so remove the degs from the list having these 3 genes and make the plot

###volcano plot for degs with adj p value less than 0.01
# Make a basic volcano plot
with(degs, plot(logFC, -log10(adj.P.Val), pch=20, main="Volcano plot",))
# Add colored points: red if adj.P.Val<0.01, orange of logFC>1.5, green if both)
with(subset(degs, adj.P.Val <0.01 ), points(logFC, -log10(adj.P.Val), pch=20, col="blue"))
with(subset(degs, abs(logFC)>1.5), points(logFC, -log10(adj.P.Val), pch=20, col="orange"))
with(subset(tmz_clean, adj.P.Val <0.01 & abs(logFC)>1.5), points(logFC, -log10(adj.P.Val), pch=20, col="grey60"))
DEGs.PT.ST<-subset(tmz_clean, adj.P.Val <0.01 & abs(logFC)>1.5)
write.table(tmz_clean,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs_primary_vs_secondary_gbm_logfc1.5_adpval_0.01.txt",sep="\t",row.names=T)
## heatmap using gtscripts of Pierre
library(GTscripts)
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm) <- rownames(batch)

### pheatmap with GTscripts
### removing the genes that are for TMZ
### plotting DEGs removing the TMZ resistant genes
ltpm<-log2(gbm_tpm+1)
rgheatmap(ltpm[rownames(tmz_clean),], scale="row",annotation=batch, fontsize=5.5,clustering_distance_cols="euclidean",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/hm-degs_pri_vs_rec_gbm_clean.png")

###Identifying TFs enriched as DEGs between Primary vs Secondary Tumor in GBM



