###Assessing the PGDRT vs PDGRT with DESeq2 with multifactorial design
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
gbm.counts.round<-round(gbm.counts)

gbm.pgrt.pdgrt<-gbm.counts.round[,c("udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]

keep <- rowSums(gbm.pgrt.pdgrt)>0
gbm.pgrt.pdgrt <- gbm.pgrt.pdgrt[keep,]

batch.pgrt.pdgrt<-batch[colnames(gbm.pgrt.pdgrt),]

rld <- rlogTransformation(dds)
hist(assay(rld))
sampleDists <- as.matrix(dist(t(assay(rld))))

write.table(batch.pgrt.pdgrt,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.pgrt.pdgrt.txt",sep="\t",row.names=T)

batch.pgrt.pdgrt <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.pgrt.pdgrt.txt",header=T,row.names=1)


### PCAPlot of the pgrt vs pdgrt
## normal pca that shows the separation by batch effects
dge <- DGEList(counts= gbm.pgrt.pdgrt.exp) ## 21285    18 for gbm.pgrt.pdgrt.exp
dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)
pca= prcomp( logCPM)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.pgrt.pdgrt$batch))
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.pgrt.pdgrt$cellTYPE))

mm <-model.matrix(~batch.pgrt.pdgrt$cellTYPE)
logCPM.corrected.combat <- sva::ComBat(logCPM, batch=as.factor(batch.pgrt.pdgrt$batch), mod = mm[,1])
pca= prcomp( logCPM.corrected.combat)
#plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.pgrt.pdgrt$cellTYPE)) ## removing the batch with combat
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",pch=19)
points(pca$rotation[1:7,], col = "#777711",pch=19)
points(pca$rotation[8:18,], col = "#AAAA44",pch=19)
library("factoextra")
 head(unclass(pca$rotation)[, 1:4])
eig <- (pca$sdev)^2
variance <- eig*100/sum(eig)
cumvar <- cumsum(variance)
eig.decathlon2.active <- data.frame(eig = eig, variance = variance,cumvariance = cumvar)
head(eig.decathlon2.active)
eig.val <- get_eigenvalue(pca)
head(eig.val)
fviz_screeplot(pca, ncp=10)
fviz_pca_var(pca)


### Running DESeq2 with MF design
####

colData <- batch.pgrt.pdgrt[,c("patient","cellTYPE")]
dds <- DESeqDataSetFromMatrix(countData = gbm.pgrt.pdgrt,colData = colData,design = ~ patient + cellTYPE)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh<-subset(resOrdered,padj<0.05) ## no DEGs

### Trying to plot the variances of the 48 exclusive DEGs found from ZvsPGRT and ZvsPDGRT that assess the differences of PGRT vs PDGRT

DEGs.exclu.up.inPGRT <- c("BNC2","APOBEC3C","HNRNPA1P10","BICC1","NQO1","TEX41","PDGFRA","PALMD","SLC7A2","ADAMTS15","JUP","DNAAF3","CLMP","PPARGC1B","SNORD17","ECSCR","DRD1","TDRD1")


DEGs.exclu.down.inPGRT <- c("WNK4","MDGA2","POSTN","ICAM1","DMTN","CHGB","ANO1","HSPB3","DCLK1","SPOCK2","TMEM63C","SULF1","GCHFR","JAG2","CLDN4","DDO","LOC284648","PLD5","ICAM5","ETNK2","CLEC18A","ANKRD24","LY96","SCAMP5","C10orf35","TM6SF2","ST3GAL6","CELSR3","MRPL42P5","MMP17")

DEGs.exclu.Z.PGRT.PDGRT<-c("BNC2","APOBEC3C","HNRNPA1P10","BICC1","NQO1","TEX41","PDGFRA","PALMD","SLC7A2","ADAMTS15","JUP","DNAAF3","CLMP","PPARGC1B","SNORD17","ECSCR","DRD1","TDRD1","WNK4","MDGA2","POSTN","ICAM1","DMTN","CHGB","ANO1","HSPB3","DCLK1","SPOCK2","TMEM63C","SULF1","GCHFR","JAG2","CLDN4","DDO","LOC284648","PLD5","ICAM5","ETNK2","CLEC18A","ANKRD24","LY96","SCAMP5","C10orf35","TM6SF2","ST3GAL6","CELSR3","MRPL42P5","MMP17")

ltpm.gbm.pgrt.pdgrt<-ltpm.gbm.all[,c("udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]


keep <- rowSums(ltpm.gbm.pgrt.pdgrt)>0
ltpm.gbm.pgrt.pdgrt.exp <- ltpm.gbm.pgrt.pdgrt[keep,]

batch.pgrt.pdgrt<-batch.gbm.new[colnames(ltpm.gbm.pgrt.pdgrt.exp),]

degs.ltpm.gbm.pgrt.pdgrt.df<-ltpm.gbm.pgrt.pdgrt[(rownames(ltpm.gbm.pgrt.pdgrt) %in% DEGs.exclu.Z.PGRT.PDGRT),]

col=colorpanel(120, "dark blue", "white","red")

pheatmap(degs.ltpm.gbm.pgrt.pdgrt.df, scale="row",annotation=batch.pgrt.pdgrt, fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = T)




gbm.z.pgrt.rz.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.91rz","udine.118rz","udine.132rz1","udine.132rz2")]

keep <- rowSums(gbm.z.pgrt.rz.tpm)>0
gbm.z.pgrt.rz.tpm.exp <- gbm.z.pgrt.rz.tpm[keep,]

gbm.z.pgrt.pdgrt.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]

degs.gbm.z.pgrt.pdgrt.df<-gbm.z.pgrt.pdgrt.tpm[(rownames(gbm.z.pgrt.pdgrt.tpm) %in% DEGs.exclu.Z.PGRT.PDGRT),]

write.table(degs.gbm.z.pgrt.pdgrt.df,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/48.degs.gbm.z.pgrt.pdgrt.xlsx",sep="\t",row.names=T)


degs.gbm.z.pgrt.pdgrt.df$var_z<- apply(degs.gbm.z.pgrt.pdgrt.df[,1:6], 1, var)
degs.gbm.z.pgrt.pdgrt.df$var_pgrt<-apply(degs.gbm.z.pgrt.pdgrt.df[,7:13], 1, var)
degs.gbm.z.pgrt.pdgrt.df$var_pdgrt<-apply(degs.gbm.z.pgrt.pdgrt.df[,14:24], 1, var)

d1 <- density(log2(degs.gbm.z.pgrt.pdgrt.df$var_z+1))
d2 <- density(log2(degs.gbm.z.pgrt.pdgrt.df$var_pgrt+1))
d3 <- density(log2(degs.gbm.z.pgrt.pdgrt.df$var_pdgrt+1))

par(mfrow=c(2,2))
plot(d1, main="Variance Density of 48 DEGs in PC")
abline(v = median(log2(degs.gbm.z.pgrt.pdgrt.df$var_z+1)),
       col = "red",
       lwd = 2)
plot(d2, main="Variance Density of 48 DEGs in PGRT")
abline(v = median(log2(degs.gbm.z.pgrt.pdgrt.df$var_pgrt+1)),
       col = "red",
       lwd = 2)
plot(d3, main="Variance Density of 48 DEGs in PDGRT")
abline(v = median(log2(degs.gbm.z.pgrt.pdgrt.df$var_pdgrt+1)),
       col = "red",
       lwd = 2)

##overlay variance density of the 48 DEGs in PC vs PGRT
plot(d1, main="Variance Density of 48 DEGs in PC , PGRT and PDGRT",col="red",ylim=c(0,2))
lines (d2,col="blue")
lines(d3,col="green")




DEGs.exclu.up.inPDGRT.Z<-c("KITLG","PAG1","ARHGEF33","PIPSL","NCKAP5","FAM35DP","GRPR","PABPC1P2","CHRNA7","STC1","C10orf90","F2RL2","ZCCHC5","SNTB1","CDYL2","PEAR1","CAMK1G","KCNS1","EVI2B","ZP1","CSF1R","CIDEB","SPTY2D1-AS1","IQSEC3","KRT9","SEMA3D","SLC6A1","MIR6132","MIR614","PTGER2","TRPV6","EPHA7")

DEGs.exclu.down.inPDGRT.Z<-c("NPAS3","C1QL1","TMEM163","CLU","CTNND2","MMP28","C1orf226","EGR2","IGSF3","SHOX2","CYP27A1","SOX21-AS1","GLB1L2","RHOU","PLP1","COL14A1","GATM","HKDC1","WSCD1","RGMA","PIFO","DNAH9","TPPP3","LPPR5","DGCR5","EPHB2","CADPS","SOX2","ISL2","TP73","SLCO5A1","CSTA","LPL","ENPP5","TSPAN7","EYA4","EFR3B","PKP2","PCDHB7","SLC38A3","PRKCQ-AS1","NGEF","MLC1","PLA2G5","HRCT1","NAT8L","ELMOD1","CNTNAP3","FABP7","CNR1","MDFI","CD34","ABCC3","CHST1","CXCL14","VANGL2","NUDT10","RNF144A","PDE3B","ADCY2","KLHL13","PCSK1","FAM134B","SLC38A5","ADRA1B","NUP210","PCDHGC4","DCLK2","CCDC88C","ILDR2","LRP1B","HOXB2","PMP22","PDLIM4","NTN1","SCX","PLEKHS1","PCDHB2","DHRS3","LYPD6","EPHA3","NMU","DKK1","FGFR3","RASGRP3","B3GAT1","RNF128","MCHR1","AP3B2","COL22A1","PLLP","CPVL","CALY","OTX1","HS3ST3A1","NOVA1","ENTPD2","ERCC-00108","TRIL","ID1","SOX9","NKX2-5","GRIK2","SEMA3B","C8orf46","MAOB","NDNF","HAND2","IMPA2","LRRN1","DAB1","F11R","ID2","ANO4","B4GALT6","HS6ST2","TF","L3MBTL4","CX3CL1","FAM84B","ADAMTS16","HUNK","SLC25A18","PHYHIPL","TRPM3","UNC93A","CSDC2","PRR15","IGDCC3","KCNF1","ASRGL1","FHOD3","LCTL","TSPAN2","CXCR4","C7orf57","ZNF536","TSPAN11","FZD5","NMNAT3","HAGLR","PDK4","HAS3","ASTN1","CYP39A1","MYO16","OLFM1","NOXA1","LINC00475","KAZALD1","CXADR","TPD52","DUSP15","MAP2K6","BMPR1B","GRIA4","BRSK2","MALL","HOXB7","ACKR3","PCDHB12","PRKCQ","CD24","CRLF1","IL7","MGC12916","TMEFF2","GADD45G","CACNA1G","LINC00461","SNTG1","ZNF436","FAM198A","SLITRK6","PITX1","PCYT1B","KIAA1161","EFNA2","MMP15","CGN","SIAH3","MX1","MTTP","EBF4","CYTL1","CACNA2D3","PAX9","LINC00693","FMNL1","NEFL","BEAN1","NFE2L3","PLEKHA6","TOB1-AS1","SEPTIN4","NELL2","LINC00639","CEND1","TMEM179","LOC100505942","STK32A","RBM47","DRAXIN","SNX10","CLCN2","RPS6KA1","PKIA","RPE65","TTC23L","HIVEP3","SDK1","ROR2","VAT1L","PPP2R2B","ARHGEF26","TFAP2A-AS1","SH3GL2","CACNG4","NAT16","MARCH10","LINC00856","FAM78B","SEPTIN3","EEPD1","CSMD2","FAM221A","NR3C2","PDGFB","THRB","LOC90768","AQP7P1","CTSH","PROS1","SLC16A9","PCDHB15","ERCC-00130","PCDH17","GNG2","SCARA3","MYBPHL","TNFSF10","ATP6V0E2","KCNMB2","MOXD1","APC2","IRS2","GPR126","TEKT2","IGSF11","FAM187A","ACY3","GNG4","MRAP2","TTTY14","ANO5","BRINP1","MCF2L","AK8","EMID1","CLIC2","NOL4","S100A16","TSPAN33","KCNIP3","AMH","FGF12","EFEMP1","ARSF","SESN3","PALM3","LOC101927497","PTCHD4","CHRNA3","LYPD6B","SCN3A","PSPN","FGF13","ANKRD20A2","SPTSSB","MSLN","GPR133","CNTNAP3B","GPR68","GALNT12","ERCC-00092","MFSD2A","SLC1A3","KIAA0226L","FAM65B","BMP7","SNAP25","ZDHHC23","SNAI1","VGF","RIMKLA","LRRTM2","OLIG1","JAKMIP2-AS1","NTNG1","SEL1L3","EEF1A2","GALNT13","BCAM","CHD5","C5orf49","C1orf106","CLDN2","CHI3L1","ALK","NTN5","SLC40A1","ANXA2R","VWCE","NOVA2","LRRC10B","TNFRSF21","ERCC-00131","HRNR","NPNT","PCDH7","UNC5D","SLC22A23","PURG","AQP7P3","RHBDL3","SRSF12","ARHGAP25","SOX5","ERCC-00136","DPYSL3","TENM1","SYNC","KIAA1598","RAB26","ITPKA","NACAD","PTN","GRIK4","SERPINA3","PLCD3","GLIPR2","SPECC1","WAS","MESP1","CSPG5","SH3PXD2B","MYO5C","LRFN1","PRR18","RBM38","ROM1","MREG","ABTB2","TMTC4","SLC35F2","RND1","RASL11B","WNT3","MID1","MAP3K9","C12orf76","ADD2","ASAP3","BACE2","IL18R1","PLXNB1","CDKN1C","GRIA3","DACT1","VWA1","MFSD3","LOC729737","TNIP3","SMAD9","SERPINE2","PCDHB16","IPCEF1","TMEM154","RCOR2","CCDC85C","SDC3","CLSTN3","MORN3","SRCIN1","FOXO1","UPP1","RASEF","C4B","C4B_2","ATP6V0E2-AS1","SORCS3","TCF7","SSX2IP","LHX2","TMCC3","CABLES1","UCHL1","SLC19A1","POPDC3","MFI2-AS1","PLXNB3","CD70","CCND2","DBP","FAM69B","ARHGEF3","FAM124A","LOC401052","GMDS-AS1","C4A","RPLP0P2","ANKRD13B","C5orf38","COL16A1","CBX2","MFAP4","TMEM253","OSBPL10","LGALS9","KCNN3","CDO1","EFNB2","F12","CACNA2D2","GSAP","PPP1R3G","BVES","HS6ST1","SDC1","ARAP2","CRABP2","FAXC","KCTD4","COL11A2","PODNL1","ECI1","DAPK2","SLC25A27","SIPA1L2","PRKCZ","DACH1","LRFN4","PARD6A","FHDC1","C9orf16","CDON","CADM4","RGS2","CPEB1","TRIM16L","TMEM26","HES6","INPP5J","REEP6","AQP11","RDH10","CTC-338M12.4","CCNJL","ABCA3","TSPAN15","TTC39C","ZNF365","MYH3","PAQR4","LTBP4","SEMA4B","NME3","GPR153","B4GALNT1","TTYH3","ARHGAP33","PPP2R3B","SSR4P1","IL27RA","KANK1","PCAT6","LPHN1","PLA2G6","PCDHGC3","PPM1E","PGM2L1","HAGHL","LMO4")

test.degs<-c("PPP2R3B","SSR4P1","IL27RA","KANK1","PCAT6","LPHN1","PLA2G6","PCDHGC3","PPM1E","PGM2L1","HAGHL","LMO4")

df1<-gbm.z.pgrt.pdgrt.tpm[(rownames(gbm.z.pgrt.pdgrt.tpm) %in% DEGs.exclu.down.inPDGRT.Z),]

df2<-gbm.z.pgrt.pdgrt.tpm[(rownames(gbm.z.pgrt.pdgrt.tpm) %in% test.degs),]

df.down.degs.482.inPDGRT.PGRT.Z<-rbind(df1,df2)

df.down.degs.482.inPDGRT.PGRT.Z$var_z<- apply(df.down.degs.482.inPDGRT.PGRT.Z[,1:6], 1, var)
df.down.degs.482.inPDGRT.PGRT.Z$var_pgrt<-apply(df.down.degs.482.inPDGRT.PGRT.Z[,7:13], 1, var)
df.down.degs.482.inPDGRT.PGRT.Z$var_pdgrt<-apply(df.down.degs.482.inPDGRT.PGRT.Z[,14:24], 1, var)


d1 <- density(log2(df.down.degs.482.inPDGRT.PGRT.Z$var_z+1))
d2 <- density(log2(df.down.degs.482.inPDGRT.PGRT.Z$var_pgrt+1))
d3 <- density(log2(df.down.degs.482.inPDGRT.PGRT.Z$var_pdgrt+1))

par(mfrow=c(2,2))
plot(d1, main="Variance Density of 481 DEGs in PC")
abline(v = median(log2(df.down.degs.482.inPDGRT.PGRT.Z$var_z+1)),
       col = "red",
       lwd = 2)
plot(d2, main="Variance Density of 481 DEGs in PGRT")
abline(v = median(log2(df.down.degs.482.inPDGRT.PGRT.Z$var_pgrt+1)),
       col = "red",
       lwd = 2)
plot(d3, main="Variance Density of 481 DEGs in PDGRT")
abline(v = median(log2(df.down.degs.482.inPDGRT.PGRT.Z$var_pdgrt+1)),
       col = "red",
       lwd = 2)



df.PDGRT.degs.32.up.inPGDR.Z<-degs.z.pdgrt_clean[(rownames(degs.z.pdgrt_clean) %in% DEGs.exclu.up.inPDGRT.Z),]
d1 <- density(df.PDGRT.degs.32.up.inPGDR.Z$log2FoldChange)
plot(d1, main="Density FoldChange of exclusive up in PGDRT from PC and also PGRT vs PDGRT from Z",col="red")


df1<-degs.z.pdgrt_clean[(rownames(degs.z.pdgrt_clean) %in% DEGs.exclu.down.inPDGRT.Z),]

df2<-degs.z.pdgrt_clean[(rownames(degs.z.pdgrt_clean) %in% test.degs),]

df.PDGRT.degs.482.down.inPGDRT.Z<-rbind(df1,df2)

d2 <- density(df.PDGRT.degs.482.down.inPGDRT.Z$log2FoldChange)
plot(d2, main="Density FoldChange of exclusive down in PGDRT from PC and also PGRT vs PDGRT from Z",col="blue")

### Plotting the variance map of the 481 and 32 genes that are up in PDGRT and down in PDGRT exclusively, their behavior density plot of log2(var+1) in Z, PGRT and PDGRT
head(df.down.degs.482.inPDGRT.PGRT.Z)
df.PDGRT.degs.32.up.inPDGRT.Z<-gbm.z.pgrt.pdgrt.tpm[(rownames(gbm.z.pgrt.pdgrt.tpm) %in% DEGs.exclu.up.inPDGRT.Z),]
df.down.degs.483.inPDGRT.PGRT.Z<-df.down.degs.482.inPDGRT.PGRT.Z[,c(1:24)]
df.degs.515.exclu.PDGRT.PGRT.Z<-rbind(df.PDGRT.degs.32.up.inPDGRT.Z,df.down.degs.483.inPDGRT.PGRT.Z)


df.degs.515.exclu.PDGRT.PGRT.Z$var_z<- apply(df.degs.515.exclu.PDGRT.PGRT.Z[,1:6], 1, var)
df.degs.515.exclu.PDGRT.PGRT.Z$var_pgrt<-apply(df.degs.515.exclu.PDGRT.PGRT.Z[,7:13], 1, var)
df.degs.515.exclu.PDGRT.PGRT.Z$var_pdgrt<-apply(df.degs.515.exclu.PDGRT.PGRT.Z[,14:24], 1, var)

d1 <- density(log2(df.degs.515.exclu.PDGRT.PGRT.Z$var_z+1))
d2 <- density(log2(df.degs.515.exclu.PDGRT.PGRT.Z$var_pgrt+1))
d3 <- density(log2(df.degs.515.exclu.PDGRT.PGRT.Z$var_pdgrt+1))

par(mfrow=c(2,2))
plot(d1, main="Variance Density of 515 DEGs in PC")
abline(v = median(log2(df.degs.515.exclu.PDGRT.PGRT.Z$var_z+1)),
       col = "red",
       lwd = 2)
plot(d2, main="Variance Density of 515 DEGs in PGRT")
abline(v = median(log2(df.degs.515.exclu.PDGRT.PGRT.Z$var_pgrt+1)),
       col = "red",
       lwd = 2)
plot(d3, main="Variance Density of 515 DEGs in PDGRT")
abline(v = median(log2(df.degs.515.exclu.PDGRT.PGRT.Z$var_pdgrt+1)),
       col = "red",
       lwd = 2)

##overlay variance density of the 515 DEGs in PC vs PDGRT
plot(d1, main="Variance Density of 515 DEGs in PC , PGRT and PDGRT",col="red",ylim=c(0,2))
lines (d2,col="blue")
lines(d3,col="green")

###overlaying of foldchanges
resOrdered.pgrt.pdgrt<-resOrdered

### heatmap of the 515 degs that are up and down in PDGRT and if they classify PC vs PDGRT
gbm.pgrt.pdgrt.tpm[rownames(df.degs.515.exclu.PDGRT.PGRT.Z),],

dim(gbm_tpm)
#gbm.pgrt.pdgrt.tpm<-gbm_tpm[,c("udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]

# pheatmap(gbm.pgrt.pdgrt.tpm[rownames(df.degs.515.exclu.PDGRT.PGRT.Z),], scale="row",annotation=batch.pgrt.pdgrt[,2:3], fontsize=7,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,color = col,show_colnames = F) # does not work as there is NaN/Inf values

#remove the genes with no SD
gbm.z.pdgrt.tpm

pheatmap(gbm.z.pdgrt.tpm[rownames(df.degs.515.exclu.PDGRT.PGRT.Z),], scale="row",annotation=batch.z.pdgrt[,3:5], fontsize=7,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,color = col,show_colnames = F)


### For heatmap that will give genes that are exclusively changing between PGRT and PDGRT consider the genes exclusively up in PGRT (18)  and up in PDGRT (32) while the ones exclusively down in PGRT (30) and down in PDGRT (483). Plot heatmap for both separately

dim(degs1.exlu.up.inPDGRT.Z)
dim(degs.18.up.exclu.PGRT.PDGRT.Z)

df.18.degs<-gbm.pgrt.pdgrt.tpm[(rownames(gbm.pgrt.pdgrt.tpm) %in% degs.18.up.exclu.PGRT.PDGRT.Z),]
df.32.degs<-gbm.pgrt.pdgrt.tpm[(rownames(gbm.pgrt.pdgrt.tpm) %in% degs1.exlu.up.inPDGRT.Z),]

df1.degs.pgrt.pdgrt<-rbind(df.18.degs,df.32.degs)


pheatmap(df1.degs.pgrt.pdgrt, scale="row",annotation=batch.pgrt.pdgrt[,2:4], fontsize=7,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F) ## not working well with the tpm or log2(tpm)

### lets try to used the batch corrected CPM data here to plot the heatmap


df.18.degs<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% degs.18.up.exclu.PGRT.PDGRT.Z),]
df.32.degs<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% DEGs.exclu.up.inPDGRT.Z),]

df1.degs.pgrt.pdgrt<-rbind(df.18.degs,df.32.degs)
pheatmap(df1.degs.pgrt.pdgrt, scale="row",annotation=batch.pgrt.pdgrt[,2:4], fontsize=7,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F,clustering_method = "complete") ## still finds a pattern but still noisy

### now trying to capture the same with 30 down in PGRT and 483 down in PDGRT over the logCPM batch corrected value

degs.30.down.inPGRT<-c("WNK4","MDGA2","POSTN","ICAM1","DMTN","CHGB","ANO1","HSPB3","DCLK1","SPOCK2","TMEM63C","SULF1","GCHFR","JAG2","CLDN4","DDO","LOC284648","PLD5","ICAM5","ETNK2","CLEC18A","ANKRD24","LY96","SCAMP5","C10orf35","TM6SF2","ST3GAL6","CELSR3","MRPL42P5","MMP17")

degs.1down.inPDGRT<-c("NPAS3","C1QL1","TMEM163","CLU","CTNND2","MMP28","C1orf226","EGR2","IGSF3","SHOX2","CYP27A1","SOX21-AS1","GLB1L2","RHOU","PLP1","COL14A1","GATM","HKDC1","WSCD1","RGMA","PIFO","DNAH9","TPPP3","LPPR5","DGCR5","EPHB2","CADPS","SOX2","ISL2","TP73","SLCO5A1","CSTA","LPL","ENPP5","TSPAN7","EYA4","EFR3B","PKP2","PCDHB7","SLC38A3","PRKCQ-AS1","NGEF","MLC1","PLA2G5","HRCT1","NAT8L","ELMOD1","CNTNAP3","FABP7","CNR1","MDFI","CD34","ABCC3","CHST1","CXCL14","VANGL2","NUDT10","RNF144A","PDE3B","ADCY2","KLHL13","PCSK1","FAM134B","SLC38A5","ADRA1B","NUP210","PCDHGC4","DCLK2","CCDC88C","ILDR2","LRP1B","HOXB2","PMP22","PDLIM4","NTN1","SCX","PLEKHS1","PCDHB2","DHRS3","LYPD6","EPHA3","NMU","DKK1","FGFR3","RASGRP3","B3GAT1","RNF128","MCHR1","AP3B2","COL22A1","PLLP","CPVL","CALY","OTX1","HS3ST3A1","NOVA1","ENTPD2","ERCC-00108","TRIL","ID1","SOX9","NKX2-5","GRIK2","SEMA3B","C8orf46","MAOB","NDNF","HAND2","IMPA2","LRRN1","DAB1","F11R","ID2","ANO4","B4GALT6","HS6ST2","TF","L3MBTL4","CX3CL1","FAM84B","ADAMTS16","HUNK","SLC25A18","PHYHIPL","TRPM3","UNC93A","CSDC2","PRR15","IGDCC3","KCNF1","ASRGL1","FHOD3","LCTL","TSPAN2","CXCR4","C7orf57","ZNF536","TSPAN11","FZD5","NMNAT3","HAGLR","PDK4","HAS3","ASTN1","CYP39A1","MYO16","OLFM1","NOXA1","LINC00475","KAZALD1","CXADR","TPD52","DUSP15","MAP2K6","BMPR1B","GRIA4","BRSK2","MALL","HOXB7","ACKR3","PCDHB12","PRKCQ","CD24","CRLF1","IL7","MGC12916","TMEFF2","GADD45G","CACNA1G","LINC00461","SNTG1","ZNF436","FAM198A","SLITRK6","PITX1","PCYT1B","KIAA1161","EFNA2","MMP15","CGN","SIAH3","MX1","MTTP","EBF4","CYTL1","CACNA2D3","PAX9","LINC00693","FMNL1","NEFL","BEAN1","NFE2L3","PLEKHA6","TOB1-AS1","Sep-04","NELL2","LINC00639","CEND1","TMEM179","LOC100505942","STK32A","RBM47","DRAXIN","SNX10","CLCN2","RPS6KA1","PKIA","RPE65","TTC23L","HIVEP3","SDK1","ROR2","VAT1L","PPP2R2B","ARHGEF26","TFAP2A-AS1","SH3GL2","CACNG4","NAT16","Mar-10","LINC00856","FAM78B","Sep-03","EEPD1","CSMD2","FAM221A","NR3C2","PDGFB","THRB","LOC90768","AQP7P1","CTSH","PROS1","SLC16A9","PCDHB15","ERCC-00130","PCDH17","GNG2","SCARA3","MYBPHL","TNFSF10","ATP6V0E2","KCNMB2","MOXD1","APC2","IRS2","GPR126","TEKT2","IGSF11","FAM187A","ACY3","GNG4","MRAP2","TTTY14","ANO5","BRINP1","MCF2L","AK8","EMID1","CLIC2","NOL4","S100A16","TSPAN33","KCNIP3","AMH","FGF12","EFEMP1","ARSF","SESN3","PALM3","LOC101927497","PTCHD4","CHRNA3","LYPD6B","SCN3A","PSPN","FGF13","ANKRD20A2","SPTSSB","MSLN","GPR133","CNTNAP3B","GPR68","GALNT12","ERCC-00092","MFSD2A","SLC1A3","KIAA0226L","FAM65B","BMP7","SNAP25","ZDHHC23","SNAI1","VGF","RIMKLA","LRRTM2","OLIG1","JAKMIP2-AS1","NTNG1","SEL1L3","EEF1A2","GALNT13","BCAM","CHD5","C5orf49","C1orf106","CLDN2","CHI3L1","ALK","NTN5","SLC40A1","ANXA2R","VWCE","NOVA2","LRRC10B","TNFRSF21","ERCC-00131","HRNR","NPNT","PCDH7","UNC5D","SLC22A23","PURG","AQP7P3","RHBDL3","SRSF12","ARHGAP25","SOX5","ERCC-00136","DPYSL3","TENM1","SYNC","KIAA1598","RAB26","ITPKA","NACAD","PTN","GRIK4","SERPINA3","PLCD3","GLIPR2","SPECC1","WAS","MESP1","CSPG5","SH3PXD2B","MYO5C","LRFN1","PRR18","RBM38","ROM1","MREG","ABTB2","TMTC4","SLC35F2","RND1","RASL11B","WNT3","MID1","MAP3K9","C12orf76","ADD2","ASAP3","BACE2","IL18R1","PLXNB1","CDKN1C","GRIA3","DACT1","VWA1","MFSD3","LOC729737","TNIP3","SMAD9","SERPINE2","PCDHB16","IPCEF1","TMEM154","RCOR2","CCDC85C","SDC3","CLSTN3","MORN3","SRCIN1","FOXO1","UPP1","RASEF","C4B","C4B_2","ATP6V0E2-AS1","SORCS3","TCF7","SSX2IP","LHX2","TMCC3","CABLES1","UCHL1","SLC19A1","POPDC3","MFI2-AS1","PLXNB3","CD70","CCND2","DBP","FAM69B","ARHGEF3","FAM124A","LOC401052","GMDS-AS1","C4A","RPLP0P2","ANKRD13B","C5orf38","COL16A1","CBX2","MFAP4","TMEM253","OSBPL10","LGALS9","KCNN3","CDO1","EFNB2","F12","CACNA2D2","GSAP","PPP1R3G","BVES","HS6ST1","SDC1","ARAP2","CRABP2","FAXC","KCTD4","COL11A2","PODNL1")

degs.2down.inPDGRT<-c("ECI1","DAPK2","SLC25A27","SIPA1L2","PRKCZ","DACH1","LRFN4","PARD6A","FHDC1","C9orf16","CDON","CADM4","RGS2","CPEB1","TRIM16L","TMEM26","HES6","INPP5J","REEP6","AQP11","RDH10","CTC-338M12.4","CCNJL","ABCA3","TSPAN15","TTC39C","ZNF365","MYH3","PAQR4","LTBP4","SEMA4B","NME3","GPR153","B4GALNT1","TTYH3","ARHGAP33","PPP2R3B","SSR4P1","IL27RA","KANK1","PCAT6","LPHN1","PLA2G6","PCDHGC3","PPM1E","PGM2L1","HAGHL","LMO4")


df1<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% degs.30.down.inPGRT),]
df2<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% degs.1down.inPDGRT),]
df3<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% degs.2down.inPDGRT),]


df2.degs.pgrt.pdgrt<-rbind(df1,df2,df3)

pheatmap(df2.degs.pgrt.pdgrt, scale="row",annotation=batch.pgrt.pdgrt[,2:4], fontsize=7,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,color = col,show_colnames = F,clustering_method = "complete",cellwidth =20)

df3.degs.pgrt.pdgrt.total<-rbind(df1.degs.pgrt.pdgrt,df2.degs.pgrt.pdgrt) ## plotting the overall degs in PGRT and PDGRT

pheatmap(df3.degs.pgrt.pdgrt.total, scale="row",annotation=batch.pgrt.pdgrt[,2:4], fontsize=8,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,color = col,show_colnames = F,clustering_method = "average",clustering_distance_rows = "correlation",cellwidth =15)

### 515 degs had ONC<- ACKR3,FGFR3,PDGFB,SOX2,ALK,CCND2,FOXO1,RAB26,SPECC1,CDON
#TSG<-WAS



TF.upstream.48.degs.z.pgrt<-c("ZNF263","ESR1","ESR2","RREB1","HIC1","TBX5","EGR1","FOSL1","SP1","KLF16","KLF5","NR2F6","SP3","MAFG","MAX::MYC","TEAD1","HIC2","TFAP2C","STAT5A::STAT5B","PLAG1","JUNB")


TF.upstream.515.degs.z.pdgrt<-c("EGR1","E2F3","EGR3","E2F4","SP1","ZIC3","ZIC1","SP2","ZIC4","TFAP2A","ZNF740","GLIS2","E2F1","TFAP2C","KLF16","EGR4","TFAP2B","E2F6","KLF5","INSM1","SP3","ZNF263","TCFL5","SP8","HINFP","NHLH1","KLF4","KLF14","NR2C2","NFKB1","PLAG1","HEY1","PAX5","RREB1","HES1","MZF1","MYOD1","HES5","MYOG","TCFCP2L1","MYCN","HES7","TCF3","MYC","HEY2","ZBTB33","HIC2","NRF1","GLIS1","TCF12","SP4","CTCF","NFKB2","ESR1","GCM2","ARNT::HIF1A","GLIS3","GCM1","KLF1","ZBTB7A","ASCL2","EWSR1-FLI1","ZFX","ZBTB7B","ZBTB7C","TCF4","AHR::ARNT","THAP1","CENPB","PPARG::RXRA","GMEB1","ESR2","EGR2")

df.TF.48<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% TF.upstream.48.degs.z.pgrt),]
df.TF.515<-logCPM.pgrt.pdgrt.combat.corrected[(rownames(logCPM.pgrt.pdgrt.combat.corrected) %in% TF.upstream.515.degs.z.pdgrt),]


pheatmap(df.TF.48, scale="row",annotation=batch.pgrt.pdgrt[,2:4], fontsize=10,clustering_distance_cols="manhattan",show_rownames = T,treeheight_col=40,color = col,show_colnames = F,clustering_method = "complete",cellwidth =20)


pheatmap(df.TF.515, scale="row",annotation=batch.pgrt.pdgrt[,2:4], fontsize=8,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F,clustering_method = "complete",cellwidth =20)