library("DESeq2")
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
gbm.counts.round<-round(gbm.counts)
gbm.z.pdgrt<-gbm.counts.round[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]
dim(gbm.z.pdgrt)
keep <- rowSums(gbm.z.pdgrt)>0
gbm.z.pdgrt <- gbm.z.pdgrt[keep,] # 21335

batch.z.pdgrt <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.z.pdgrt.txt",header=T,row.names=1)
dge <- DGEList(counts= gbm.z.pdgrt)
dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)
### plotting to see if there is a batch effect or not
plotMDS(logCPM,dim.plot=c(1,2),col=as.numeric(batch.z.pdgrt$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
pca= prcomp( logCPM)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.z.pdgrt$batch))
text(pca$rotation[,1],pca$rotation[,2], row.names(pca$rotation), cex=0.4, pos=3)
mcor=cor(logCPM,method="spearman")
### plotting the above non correct logCPM correlated data
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =20, cellheight =20,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean",filename="/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/corr_euc_heatmap_bonn_zvspdgrt_before_bc.pdf")


### trying to see and remove to see if there is a batch correction needed
mm <-model.matrix(~batch.z.pdgrt$cellTYPE)
logCPM.corrected.combat <- sva::ComBat(logCPM, batch=as.factor(batch.z.pdgrt$batch), mod = mm[,1])
plotMDS(logCPM.corrected.combat,dim.plot=c(1,2),col=as.numeric(batch.z.pdgrt$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
mcor=cor(logCPM.corrected.combat,method="pearson")
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =20, cellheight =20,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean",filename="/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/corr_euc_heatmap_bonn_zvspdgrt_after_bc.pdf")
pca= prcomp( logCPM.corrected.combat)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2",col=as.numeric(batch.z.pdgrt$batch))
text(pca$rotation[,1],pca$rotation[,2], row.names(pca$rotation), cex=0.4, pos=3)

### There is a mild batch effect which is somewhat removed with combat but not all since there are too many pdgrt samples


### To be done tomorrow 
#### So running DESEQ2 without batch correction just using the condition and finding the DEGs ( this is still ok but not entirely accurate)


colData <- data.frame(row.names = colnames(gbm.z.pdgrt),condition = factor(c(batch.z.pdgrt$cellTYPE)))
colData
#mm <-model.matrix(~batch.z.pgrt$batch+batch.z.pgrt$cellTYPE)
dds <- DESeqDataSetFromMatrix(countData = gbm.z.pdgrt,colData = colData,design = ~ condition)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh<-subset(resOrdered,padj<0.05) #1114 aug 23
## Volcano plot
tmz_clean<-as.matrix(thresh[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(thresh)), ]) ## removing the tmz genes
degs.z.pdgrt_clean <-data.frame(tmz_clean) #1184 degs #1113 degs 

with(resOrdered, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1, green if both)
with(subset(resOrdered, padj <0.05 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(resOrdered, abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="orange"))
with(subset(degs.z.pdgrt_clean, padj <0.05 & abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="grey60")) ## now take these genes and plot heatmap
p1<-subset(degs.z.pdgrt_clean, padj <0.05 & abs(log2FoldChange)>1) ## heatmap of these genes 857
write.table(p1,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/deseq2.degs.zvspdgrt_no_batch_correction.aug23.txt",sep="\t",row.names=T)
##heatmap
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]
gbm.z.pdgrt.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]

## heatmap
rgheatmap(gbm.z.pdgrt.tpm[rownames(p1),], scale="row",annotation=batch.z.pdgrt, fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/zvspdgrt_degs_no_batch_correction_correlation_deseq2_out.png")

###23 aug heatmap with 857 degs
ltpm<-log2(gbm.z.pdgrt.tpm+1)
rgheatmap(ltpm[rownames(p1),], scale="row",annotation=batch.z.pdgrt, fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,filename="/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/zvspdgrt_degs_no_batch_correction_correlation_deseq2_out.aug23.png")


### The above usage of removing batch effect with combat might not be the correct way to proceed as it is not clear whether om tranformaing the log combat is a correct way and then feeding to DESEQ2 so it is better to use the multifactorial of implementing the batch inside the design in order to appropriately estimate the size of batch effect and then substract it while performing tests. Since combat prefers log transformed values and then corrects for known batches and this gives negative values at times if there is tailing data on either side of distribution. So preferrably it is not a correct method as deseq expects to get raw counts with rounded off values, so better is to do multifacotorial where batch will be used in the design which will be regressed and substracted. 

### Multifactorial design with DESEQ2 for  z v pgdrt with individuals

colData <- data.frame(row.names = colnames(gbm.z.pdgrt),batch=factor(c(batch.z.pdgrt$batch)),condition = factor(c(batch.z.pdgrt$cellTYPE)))
colData
#mm <-model.matrix(~batch.z.pgrt$batch+batch.z.pgrt$cellTYPE)
dds <- DESeqDataSetFromMatrix(countData = gbm.z.pdgrt,colData = colData,design = ~ batch + condition)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh<-subset(resOrdered,padj<0.05) #1907 degs # 1792
## Volcano plot
tmz_clean<-as.matrix(thresh[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(thresh)), ]) ## removing the tmz genes
degs.z.pdgrt_clean <-data.frame(tmz_clean) #1905 degs  #1790

with(resOrdered, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1, green if both)
with(subset(resOrdered, padj <0.05 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(resOrdered, abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="orange"))
with(subset(degs.z.pdgrt_clean, padj <0.05 & abs(log2FoldChange)>1), points(log2FoldChange, -log10(padj), pch=20, col="grey60")) ## now take these genes and plot heatmap
p1<-subset(degs.z.pdgrt_clean, padj <0.05 & abs(log2FoldChange)>1) ## heatmap of these genes 548 ##840
write.table(p1,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/deseq2.degs.zvspdgrt_multifactorial_aug23.txt",sep="\t",row.names=T)
##heatmap
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]
gbm.z.pdgrt.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]

## heatmap
ltpm<-log2(gbm.z.pdgrt.tpm+1)
rgheatmap(ltpm[rownames(p1),], scale="row",annotation=batch.z.pdgrt, fontsize=5.5,clustering_distance_cols="canberra",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/zvspdgrt_degs_multifactorial_correlation_deseq2_out.png")

#heatmap
rgheatmap(gbm.z.pgrt.tpm[rownames(degs.z.pdgrt.mf.clean),], scale="row",annotation=batch.z.pgrt[,1:5], fontsize=5.5,clustering_distance_cols="maximum",show_rownames = F,treeheight_col=40,filename="/Users/vdas/Desktop/degs.z.pdgrt.clean_hm.png")


## heatmap of the 515 genes that exclusively change between PC and PGRT
degs1.exlu.up.inPDGRT.Z<-c("KITLG","PAG1","ARHGEF33","PIPSL","NCKAP5","FAM35DP","GRPR","PABPC1P2","CHRNA7","STC1","C10orf90","F2RL2","ZCCHC5","SNTB1","CDYL2","PEAR1","CAMK1G","KCNS1","EVI2B","ZP1","CSF1R","CIDEB","SPTY2D1-AS1","IQSEC3","KRT9","SEMA3D","SLC6A1","MIR6132","MIR614","PTGER2","TRPV6","EPHA7")

df1<-gbm.z.pdgrt.tpm[(rownames(gbm.z.pdgrt.tpm) %in% degs1.exlu.up.inPDGRT.Z),]

degs2.exclu.down.inPDGRT.Z<-rownames(df.PDGRT.degs.482.down.inPGDRT.Z)

df2<-gbm.z.pdgrt.tpm[(rownames(gbm.z.pdgrt.tpm) %in% degs2.exclu.down.inPDGRT.Z),]
df3<-rbind(df1,df2)

pheatmap(df3, scale="row",annotation=batch.z.pdgrt[,3:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=40,color = col,show_colnames = T)


###overlaying of foldchanges
resOrdered.z.pdgrt<-resOrdered
DEGs.exclu.up.inPDGRT.Z

### overalying of Log2FC of the 32 degs

df1<-resOrdered.z.pgrt[(rownames(resOrdered.z.pgrt) %in% DEGs.exclu.up.inPDGRT.Z),]
df2<-resOrdered.z.pdgrt[(rownames(resOrdered.z.pdgrt) %in% DEGs.exclu.up.inPDGRT.Z),]
df3<-resOrdered.pgrt.pdgrt[(rownames(resOrdered.pgrt.pdgrt) %in% DEGs.exclu.up.inPDGRT.Z),]


d1 <- density(df1$log2FoldChange) # 32 in ZvsPGRT log2FC
d2 <- density(df2$log2FoldChange) # same 32 in ZvsPDGRT log2FC
d3 <- density(df3$log2FoldChange) # same 32 in PGRT vs PDGRT log2FC

plot(d1, main="Density plot of FC for 32 DEGs up only in PDGRT",col="red",xlim=c(-1,2),ylim=c(0,4))
lines (d2,col="blue")
lines (d3,col="dark green")

### overalying of Log2FC of the 483 degs

degs2.exclu.down.inPDGRT.Z # 483 down degs exclusively in PDGRT when compared with up and down degs in Z vs PGRT and Z vs PDGRT


df1<-resOrdered.z.pgrt[(rownames(resOrdered.z.pgrt) %in% degs2.exclu.down.inPDGRT.Z),]
df2<-resOrdered.z.pdgrt[(rownames(resOrdered.z.pdgrt) %in% degs2.exclu.down.inPDGRT.Z),]
df3<-resOrdered.pgrt.pdgrt[(rownames(resOrdered.pgrt.pdgrt) %in% degs2.exclu.down.inPDGRT.Z),]


d1 <- density(df1$log2FoldChange) # 483 in ZvsPGRT log2FC
d2 <- density(df2$log2FoldChange) # same 483 in ZvsPDGRT log2FC
d3 <- density(df3$log2FoldChange) # same 483 in PGRT vs PDGRT log2FC

plot(d1, main="Density plot of FC for 483 DEGs down only in PDGRT",col="red",xlim=c(-4,2),ylim=c(0,7))
lines (d2,col="blue")
lines (d3,col="dark green")

## plotting the 32 genes up exclusively in PDGRT and 483 down exclusively down in PDGRT , so plot the heatmap in Z vs PDGRT of these genes

gbm.z.pdgrt.tpm<-gbm_tpm[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p2","udine.132p3","udine.141p2","udine.141p3","udine.141p4","udine.183p1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.168P1", "ieo.168P2")]

keep <- rowSums(gbm.z.pdgrt.tpm)>0
gbm.z.pdgrt.tpm <- gbm.z.pdgrt.tpm[keep,]

batch.z.pdgrt<-batch.gbm.new[colnames(gbm.z.pdgrt.tpm),]


degs1.exlu.up.inPDGRT.Z<-c("KITLG","PAG1","ARHGEF33","PIPSL","NCKAP5","FAM35DP","GRPR","PABPC1P2","CHRNA7","STC1","C10orf90","F2RL2","ZCCHC5","SNTB1","CDYL2","PEAR1","CAMK1G","KCNS1","EVI2B","ZP1","CSF1R","CIDEB","SPTY2D1-AS1","IQSEC3","KRT9","SEMA3D","SLC6A1","MIR6132","MIR614","PTGER2","TRPV6","EPHA7")


degs.1down.inPDGRT<-c("NPAS3","C1QL1","TMEM163","CLU","CTNND2","MMP28","C1orf226","EGR2","IGSF3","SHOX2","CYP27A1","SOX21-AS1","GLB1L2","RHOU","PLP1","COL14A1","GATM","HKDC1","WSCD1","RGMA","PIFO","DNAH9","TPPP3","LPPR5","DGCR5","EPHB2","CADPS","SOX2","ISL2","TP73","SLCO5A1","CSTA","LPL","ENPP5","TSPAN7","EYA4","EFR3B","PKP2","PCDHB7","SLC38A3","PRKCQ-AS1","NGEF","MLC1","PLA2G5","HRCT1","NAT8L","ELMOD1","CNTNAP3","FABP7","CNR1","MDFI","CD34","ABCC3","CHST1","CXCL14","VANGL2","NUDT10","RNF144A","PDE3B","ADCY2","KLHL13","PCSK1","FAM134B","SLC38A5","ADRA1B","NUP210","PCDHGC4","DCLK2","CCDC88C","ILDR2","LRP1B","HOXB2","PMP22","PDLIM4","NTN1","SCX","PLEKHS1","PCDHB2","DHRS3","LYPD6","EPHA3","NMU","DKK1","FGFR3","RASGRP3","B3GAT1","RNF128","MCHR1","AP3B2","COL22A1","PLLP","CPVL","CALY","OTX1","HS3ST3A1","NOVA1","ENTPD2","ERCC-00108","TRIL","ID1","SOX9","NKX2-5","GRIK2","SEMA3B","C8orf46","MAOB","NDNF","HAND2","IMPA2","LRRN1","DAB1","F11R","ID2","ANO4","B4GALT6","HS6ST2","TF","L3MBTL4","CX3CL1","FAM84B","ADAMTS16","HUNK","SLC25A18","PHYHIPL","TRPM3","UNC93A","CSDC2","PRR15","IGDCC3","KCNF1","ASRGL1","FHOD3","LCTL","TSPAN2","CXCR4","C7orf57","ZNF536","TSPAN11","FZD5","NMNAT3","HAGLR","PDK4","HAS3","ASTN1","CYP39A1","MYO16","OLFM1","NOXA1","LINC00475","KAZALD1","CXADR","TPD52","DUSP15","MAP2K6","BMPR1B","GRIA4","BRSK2","MALL","HOXB7","ACKR3","PCDHB12","PRKCQ","CD24","CRLF1","IL7","MGC12916","TMEFF2","GADD45G","CACNA1G","LINC00461","SNTG1","ZNF436","FAM198A","SLITRK6","PITX1","PCYT1B","KIAA1161","EFNA2","MMP15","CGN","SIAH3","MX1","MTTP","EBF4","CYTL1","CACNA2D3","PAX9","LINC00693","FMNL1","NEFL","BEAN1","NFE2L3","PLEKHA6","TOB1-AS1","Sep-04","NELL2","LINC00639","CEND1","TMEM179","LOC100505942","STK32A","RBM47","DRAXIN","SNX10","CLCN2","RPS6KA1","PKIA","RPE65","TTC23L","HIVEP3","SDK1","ROR2","VAT1L","PPP2R2B","ARHGEF26","TFAP2A-AS1","SH3GL2","CACNG4","NAT16","Mar-10","LINC00856","FAM78B","Sep-03","EEPD1","CSMD2","FAM221A","NR3C2","PDGFB","THRB","LOC90768","AQP7P1","CTSH","PROS1","SLC16A9","PCDHB15","ERCC-00130","PCDH17","GNG2","SCARA3","MYBPHL","TNFSF10","ATP6V0E2","KCNMB2","MOXD1","APC2","IRS2","GPR126","TEKT2","IGSF11","FAM187A","ACY3","GNG4","MRAP2","TTTY14","ANO5","BRINP1","MCF2L","AK8","EMID1","CLIC2","NOL4","S100A16","TSPAN33","KCNIP3","AMH","FGF12","EFEMP1","ARSF","SESN3","PALM3","LOC101927497","PTCHD4","CHRNA3","LYPD6B","SCN3A","PSPN","FGF13","ANKRD20A2","SPTSSB","MSLN","GPR133","CNTNAP3B","GPR68","GALNT12","ERCC-00092","MFSD2A","SLC1A3","KIAA0226L","FAM65B","BMP7","SNAP25","ZDHHC23","SNAI1","VGF","RIMKLA","LRRTM2","OLIG1","JAKMIP2-AS1","NTNG1","SEL1L3","EEF1A2","GALNT13","BCAM","CHD5","C5orf49","C1orf106","CLDN2","CHI3L1","ALK","NTN5","SLC40A1","ANXA2R","VWCE","NOVA2","LRRC10B","TNFRSF21","ERCC-00131","HRNR","NPNT","PCDH7","UNC5D","SLC22A23","PURG","AQP7P3","RHBDL3","SRSF12","ARHGAP25","SOX5","ERCC-00136","DPYSL3","TENM1","SYNC","KIAA1598","RAB26","ITPKA","NACAD","PTN","GRIK4","SERPINA3","PLCD3","GLIPR2","SPECC1","WAS","MESP1","CSPG5","SH3PXD2B","MYO5C","LRFN1","PRR18","RBM38","ROM1","MREG","ABTB2","TMTC4","SLC35F2","RND1","RASL11B","WNT3","MID1","MAP3K9","C12orf76","ADD2","ASAP3","BACE2","IL18R1","PLXNB1","CDKN1C","GRIA3","DACT1","VWA1","MFSD3","LOC729737","TNIP3","SMAD9","SERPINE2","PCDHB16","IPCEF1","TMEM154","RCOR2","CCDC85C","SDC3","CLSTN3","MORN3","SRCIN1","FOXO1","UPP1","RASEF","C4B","C4B_2","ATP6V0E2-AS1","SORCS3","TCF7","SSX2IP","LHX2","TMCC3","CABLES1","UCHL1","SLC19A1","POPDC3","MFI2-AS1","PLXNB3","CD70","CCND2","DBP","FAM69B","ARHGEF3","FAM124A","LOC401052","GMDS-AS1","C4A","RPLP0P2","ANKRD13B","C5orf38","COL16A1","CBX2","MFAP4","TMEM253","OSBPL10","LGALS9","KCNN3","CDO1","EFNB2","F12","CACNA2D2","GSAP","PPP1R3G","BVES","HS6ST1","SDC1","ARAP2","CRABP2","FAXC","KCTD4","COL11A2","PODNL1")

degs.2down.inPDGRT<-c("ECI1","DAPK2","SLC25A27","SIPA1L2","PRKCZ","DACH1","LRFN4","PARD6A","FHDC1","C9orf16","CDON","CADM4","RGS2","CPEB1","TRIM16L","TMEM26","HES6","INPP5J","REEP6","AQP11","RDH10","CTC-338M12.4","CCNJL","ABCA3","TSPAN15","TTC39C","ZNF365","MYH3","PAQR4","LTBP4","SEMA4B","NME3","GPR153","B4GALNT1","TTYH3","ARHGAP33","PPP2R3B","SSR4P1","IL27RA","KANK1","PCAT6","LPHN1","PLA2G6","PCDHGC3","PPM1E","PGM2L1","HAGHL","LMO4")



df1<-gbm.z.pdgrt.tpm[(rownames(gbm.z.pdgrt.tpm) %in% degs1.exlu.up.inPDGRT.Z),] ## 32
df2<-gbm.z.pdgrt.tpm[(rownames(gbm.z.pdgrt.tpm) %in% degs.1down.inPDGRT),] 
df3<-gbm.z.pdgrt.tpm[(rownames(gbm.z.pdgrt.tpm) %in% degs.2down.inPDGRT),]

df3.degs.z.pdgrt.total<-rbind(df1,df2,df3)

pheatmap(log2(df3.degs.z.pdgrt.total+1), scale="row",annotation=batch.z.pdgrt[,2:4], fontsize=8,clustering_distance_cols="binary",show_rownames = F,treeheight_col=40,color = col,show_colnames = F,clustering_method = "average",clustering_distance_rows = "correlation",cellwidth =15)

###running motif analysis on the 515 exclu degs from zvspdgrt
tx2gene <- read.table("/Users/vdas/Documents/Test_lab_projects/Pietro_Ova_data_092015/RNA-Seq/output/hg19/transcript2gene.txt",header=T)


write.table(df3.degs.z.pdgrt.total,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/515.degs.exclus.z.pdgrt.txt",sep="\t",row.names=T)

df2<-read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/515.degs.exclus.z.pdgrt.txt",header=T)

refseq.515<-merge(tx2gene,df2,by="ID")

write.table(refseq.515,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/pgrt_vs_pdgrt/515.degs.refseq.exclus.z.pdgrt.txt",sep="\t",row.names=T)


# pscan on this degs revealed 73 TFs upstream with pvalue < 0.05 which had 1 overlap with the 515 degs for EGR2.
