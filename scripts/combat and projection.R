###written by Pietro Lo Riso (last version 30/06/16)###
#load required packages

library(data.table)
library(lumi)
library(pheatmap)

#read table

        x <- fread("Desktop/betas_sites_IEO_NEB_AUS_nocorr.csv", header = T, sep = ",")
        x <- as.data.frame(x)
        
#put ID into row names and remove the column

        row.names(x) <- x$ID
        x$ID <- NULL
        
#keep only the methylation beta values

        x <- x[,5:164]
#calculate sd on rows and sort from higher to lower

        x$sd <- apply(x, 1, FUN = sd)
        master_b <- na.omit(x[order(x$sd, decreasing = T),])

#load annotation file
        
        annotation <- read.csv("Desktop/IEO_Nebraska_Australia_fine.csv", header = T, sep = ";")
       
        
#remove discarded samples from RnBeads

        annotation_final <- annotation[which(annotation$Sample_ID != "GSM1606899"),]
        
#convert beta to m-values

        master_m <- beta2m(master_b[,1:160])
        
#model matrix on Cell type and correct batch

        mm <- model.matrix(~annotation_final$Cell_type)
        master_m_corr <- sva::ComBat(master_m, batch=as.factor(annotation_final$Batch), mod = mm)

#convert back to beta-values

        master_b_corr <- m2beta(master_m_corr)

#subset table to use FI and OSE to generate the PC to be used for projection

        FI_OSE_our <- master_b_corr[,c(24:37)] #pca.from
        EOC_AS_total <- master_b_corr[,c(6:15,21:23,38:46,54:160)] #pca.new1
        EOC_total <- master_b_corr[,c(6:15,21,43:46,82:160)] #pca.new2
        AS_total <- master_b_corr[,c(22,23,38:45,54:81)] #pca.new3
        
#project data on PC found in FI and OSE
        
        pca.project(FI_OSE_our, EOC_AS_total)