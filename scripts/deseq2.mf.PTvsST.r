####DESEQ2 for PrivsSeconday GBM
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
gbm.counts.round<-round(gbm.counts)

keep <- rowSums(gbm.counts.round)>0
gbm.counts.round <- gbm.counts.round[keep,]
dim(gbm.counts.round)

colData <- batch[,c("batch","tumortypes")]
colData
#mm<- model.matrix(~batch+condition, colData)
dds <- DESeqDataSetFromMatrix(countData = gbm.counts.round,colData = colData,design = ~ batch + tumortypes)
dds
ddds <- DESeq(dds)
res <- results(ddds)
res
resOrdered<-res[order(res$padj),]
head(resOrdered)
thresh<-subset(resOrdered,padj<0.05) ## 6374

## remove tmz genes
tmz_clean<-as.matrix(thresh[!grepl("GLUT1|GLUT2|GLUT3|GLUT10|AKR1C1|AKR1C2|AKR1C3|SCRG1|IGSF4D|SLC2A10|SPP1|SLC2A5|SCG2|MDR1|PTCH1|MGMT|APNG|BER|NAMPT|BCRP1|LIF|TNFAIP3|MSH6|MSH2", rownames(thresh)), ]) ## removing the tmz genes
degs.PT.ST.clean <-data.frame(tmz_clean) # 6365
## Volcano plot
with(resOrdered, plot(log2FoldChange, -log10(padj), pch=20, main="Volcano plot",))
# Add colored points: red if padj <0.05, orange of logFC>1, green if both)
with(subset(resOrdered, padj <0.01 ), points(log2FoldChange, -log10(padj), pch=20, col="blue"))
with(subset(resOrdered, abs(log2FoldChange)>1.5), points(log2FoldChange, -log10(padj), pch=20, col="orange"))
with(subset(degs.PT.ST.clean, padj <0.01 & abs(log2FoldChange)>1.5), points(log2FoldChange, -log10(padj), pch=20, col="grey60")) ## now take these genes and plot heatmap
degs.PT.ST.final<-subset(degs.PT.ST.clean, padj <0.01 & abs(log2FoldChange)>1.5) ## 1702 DEGs
write.table(degs.PT.ST.final,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.degs.PTvsST.mf.txt",sep="\t",row.names=T)

###aug 23
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]

ltpm.gbm.all<-log2(gbm_tpm+1)

rgheatmap(ltpm.gbm.all[rownames(degs.PT.ST.final),], scale="row",annotation=batch[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = F,treeheight_col=30,show_colnames=F)

### TFs as DEGs

TFs.DEGs.PT.ST<-c("TBX18","FOXC2","POU2F2","FOXS1","CDX1","STAT4","RUNX3","ZNF560","HEYL","STAT1","FOXL1","SNAI2","HMGA2","CREB3L1","ZIC2","ARID3A","NKX6-1","MSC","EBF2","NR2F2","NFATC4","STAT6","DMRT3","OSR1","LYL1","VTN","ZFP37","GFI1B","MAFF","BACH1","CREB3L2","EBF1","ETS1","ZIC4","SIX2","NR4A3","HIVEP2","PGR","PAX2","KLF4","SP100","PLAGL1","MAFK","LBX2","GLIS1","NFIB","HIVEP1","HIC1","PRRX2","CEBPA","GLI2","ZNF257","EMX2","MAFB","KLF15","TBX15","ZNF114","ALX1","NR0B1","GLI1","NR4A1","ZFHX4","ZIC5","TBX2","HLX","SMAD1","HHEX","NKX3-2","AHR","BHLHE41","SHOX2","ZIC1","FOXL2","EBF3","PAX6","TP63","IRF6","TFAP2C","ZBTB7C","HEY1","VDR","ZNF556","ZBED2","FOXP2","DBP","ZNF469","SOX18","FOXD4L4","POU4F1","RORB","SMAD9","ID2","MSX2","FOSB","NKX2-8","MECOM","ZNF732","PITX1","KLF11","PAX3","FOXD4L1","MEIS1","PRDM6") 

TFs.DEGs.PT.ST.df<-degs.PT.ST.final[(rownames(degs.PT.ST.final) %in% TFs.DEGs.PT.ST),]

write.table(TFs.DEGs.PT.ST.df,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.TF.DEGs.PTvsST.mf.txt",sep="\t",row.names=T)

##Heatmap of TF DEGs 
ltpm.gbm<-log2(gbm_tpm+1)
col=colorpanel(120, "dark blue", "white","dark red")

pheatmap(ltpm.gbm[rownames(TFs.DEGs.PT.ST.df),], scale="row",annotation=batch[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)

### List of oncogenes that are changing in between PT vs ST
TF.Onco.DEGs.PT.ST<-c("CEBPA","CREB3L1","CREB3L2","EBF1","ETS1","FOSB","FOXL2","HEY1","HMGA2","LYL1","MAFB","MAFF","MAFK","MECOM","NFIB","NR4A3","PAX3")
TF.Onco.DEGs.PT.ST.df<-degs.PT.ST.final[(rownames(degs.PT.ST.final) %in% TF.Onco.DEGs.PT.ST),]

col=colorpanel(120, "dark blue", "black","yellow")

pheatmap(ltpm.gbm[rownames(TF.Onco.DEGs.PT.ST.df),], scale="row",annotation=batch[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)

### refseq list from DEGs
tx2gene <- read.table("/Users/vdas/Documents/Test_lab_projects/Pietro_Ova_data_092015/RNA-Seq/output/hg19/transcript2gene.txt",header=T)
DEGs.PT.ST.test <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/DEGs.PTvsST.txt",header=T)
list<-merge(DEGs.PT.ST.test,tx2gene,by="ID")
write.table(list,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/DEGs.PTvsST.refseq.txt",sep="\t",row.names=T)

#### Evaluatiing the Verhaak signatures in our GBM cohort
vk.cl<-c("CDK6","ABCD2","GAS1","TGIF2","TBX2","DOCK6","CAMK2B","CALM1","SHOX2","IRS2","GRIK1","MEIS1","ADAM19","JAG1","LHFP","TMEM147","TMED1","RIN1","PEPD","HS3ST3B1","CDH4","ACSL3","RGS6","KCNF1","LMO2","FZD7","SLC2A10","RBCK1","NPEPL1","POFUT1","MCC","FLJ11286","CD97","PLA2G5","ARNTL","ANXA5","PIPOX","LRRC16","ELOVL2","VAV3","MEOX2","FLJ21963","NR2E1","SIPA1L1","GALNT4","LRP10","LAMB2","OSBPL3","TRIP6","GSTK1","PTPN14","TEAD3","EPHB4","PCSK5","DLC1","CD151","ITGA7","M6PRBP1","RASGRP1","ACSBG1","MLC1","CENTD1","SLC4A4","FGFR3","PTPRA","ITGB8","EYA2","NES","DENND2A","DAG1","CENTD3","SPRY2","PDGFA","SLC12A4","GLG1","SSH3","PMP22","SOCS2","EGFR","SLC6A11","SLC6A9","LFNG","GNG7","SEMA6D","FZD3","BTBD2","ZNF446","DMWD","GRIK5","ZNF20","CREB5","WSCD1","TRIB2","BLM","GPR56","SEMA6A","NPAS3","SOX9","QTRT1","CC2D1A","AKT2","LRFN3","SCAMP4","C19orf22","RBM42","HMG20B","TMEM161A","APBA3","RFXANK","KEAP1","NR2F6","ERCC2","KIAA0355","HSPBP1","MGC2752","ZNF606","ZNF671","ZNF264","ZNF8","ZNF211","ZNF304","ZNF134","ZNF419","ZNF45","ZNF228","ZNF227","ZNF235","PRPF31","MEGF8","CD3EAP","PRKD2","IRF3","B3GALT1","CDH2","NOS2A","FBXO17","MAB21L1","CDH6","SEPTIN11","KLHL4","EXTL3","KLHDC8A","SMO","GLI2","RFX2","GNAS","ZFHX4","LRP5","KLHL25","RGS12","CLIP2","POMT2","SARS2","TLE2","VPS16","JUND","MYO5C","TGFB3","ILK","SNTA1","GJA1","NDP")
vk.mes<-c("UAP1","YAP1","LTBP1","SYPL1","RABGAP1L","ALDH3B1","LOX","FHL2","IGFBP6","DCBLD2","BNC2","CASP8","HEXA","COL8A2","TES","AIM1","ENG","FXYD5","LY75","PLAU","SH2B3","CDCP1","TRIM38","SP100","FES","S100A4","P4HA2","COL5A1","COL1A2","COL1A1","THBS1","IL1R1","LAMB1","LTBP2","RAB27A","HEXB","TMBIM1","COPZ2","ANXA4","PGCP","SRPX2","ZNF217","DRAM","MRC2","WWTR1","SERPINE1","CLCF1","LRRFIP1","FURIN","RBMS1","NRP1","MYH9","ADAM12","MVP","FER1L3","IQGAP1","CAST","SHC1","ITGA5","FNDC3B","GLT25D1","TCIRG1","TGOLN2","TNFRSF1A","ANXA1","PTRF","EHD2","EFEMP2","TIMP1","LGALS1","LGALS3","CHI3L1","CLIC1","CCDC109B","EMP3","ANXA2","PDPN","PYGL","ARSJ","SWAP70","TRADD","PLS3","ICAM3","VAMP5","RRAS","PROCR","RAB32","ASL","RELB","TGFBI","SLC16A3","IL15RA","ELF4","SLC10A3","GCNT1","LYPLA3","DSC2","MAPK13","HK3","MAN2B1","GRN","CEBPB","MGAT1","FLJ20273","POLD4","VDR","NOD2","DAB2","PTGER4","THBD","PLAUR","CASP5","CASP4","CASP1","SLAMF8","FLJ22662","RHOG","CTSB","CTSC","TGFBR2","IL4R","LHFPL2","GNA15","TNFRSF1B","CCR5","DOK3","BATF","RAC2","NCF2","KYNU","DSE","C1orf38","ITGAM","MS4A4A","MSR1","IFI30","NCF4","CD14","SIGLEC9","PTPN6","MYO1F","LAIR1","LAPTM5","ITGB2","TLR2","PTPRC","ALOX5","LCP1","FCGR2A","CD4","LCP2","CSTA","SERPINA1","PSCD4","MAFB","SYNGR2","STXBP2","ARPC1B","SIGLEC7","LILRB3","NPC2","TRPM2","FMNL1","LY96","SLC11A1","ECGF1","MFSD1","SQRDL","PSCDBP","S100A11","TNFAIP8","FPRL2","C5AR1","LILRB2","AMPD3","STAB1","MAN1A1","CTSZ","STAT6","FCGR2B","ST14","ACSL1","SCPEP1","ACPP","RAB11FIP1","TNFRSF11A","WIPF1","TNFAIP3","PTPN22","SAT1","PLK3","SEC24D","CLEC2B","MAN2A1","BDKRB2","CD2AP","SFT2D2","RUNX2","ITGA4","HFE","FHOD1","TRIM22","PHF11","RBKS","MGST2","C1orf54","BLVRB","CYBRD1","ARHGAP29","CNN2","TLR4","PIGP","UCP2","FOLR2","TEC","S100A13")
vk.PN<-c("CDKN1B","EPB41","CLGN","PDE10A","RALGPS2","TAF5","ACTR1A","PELI1","ZNF804A","PPM1D","RAD21","SORCS3","ALCAM","PURG","SLC1A1","NR0B1","FBXO21","GADD45G","GABRA3","FHOD3","TMSL8","LRRTM4","PAFAH1B3","FAM110B","REEP1","SNX26","LRP6","C1QL1","TMCC1","GSK3B","MMP16","LPHN3","DLL3","SEC61A2","ICK","RAP2A","ZNF286A","FAM125B","ZEB2","AMOTL2","NKX2-2","TTC3","TOP2B","SCN3A","TOX3","MLLT11","RALGPS1","GSTA4","DGKI","MTSS1","SATB1","KIF21B","PODXL2","CRMP1","GNG4","MYT1","BCL7A","CHD7","SOX4","SOX11","DCX","C6orf134","FAM77C","MARCKSL1","C1orf106","DBN1","DPYSL4","TMEFF1","STMN4","DUSP26","PAK7","PAK3","MAST1","TNRC4","CDK5R1","ATP1A3","FGF9","WASF1","CA10","RP11-35N6.1","SOX10","ERBB3","RAB33A","IL1RAPL1","DNM3","GPR17","CLASP2","PPM1E","STMN1","FLRT1","EPHB1","HDAC2","PHF16","LOC81691","CDC7","MYB","MCM10","ATAD5","HMGB3","CDC25A","TOPBP1","HN1","AOF2","SLCO5A1","NOL4","YPEL1","CXXC4","ZNF643","KLRK1","KLRC4","KLRC3","PCDH11Y","PCDH11X","HRASLS","KIAA1166","ZNF184","CASK","ZNF711","ZNF510","WDR68","MYST2","CBX1","CAMSAP1L1","HOXD3","MATR3","TMEM118","LOC55565","VAX2","TMEM35","NRXN1","SPTBN2","NCALD","SOX2","SCG3","TTYH1","C1orf61","GPM6A","DPF1","RUFY3","NRXN2","NCAM1","MAPT","SRGAP3","FXYD6","ABAT","PHLPP","OLIG2","MAP2","BCAN","NLGN3","CSPG5","DPP6","MYO10","BAI3","ASCL1","SEZ6L","CKB","CRB1","GRIA2","ARHGEF9","VEZF1","MMP15","MARCKS","BEX1","CNTN1","E2F3","RBPJ","BCOR","P2RX7","C20orf42","CSNK1E","ZNF248","PLCB4","PFN2","GPR23")
vk.NL<-c("TTPA","SIRT5","CASQ1","AKR7A3","MRPL49","GUK1","VSX1","NDRG2","PPP2R5A","RND1","ZNF323","LYRM1","SEPW1","USP33","ANKRD46","SPAST","PRPSAP2","PDE6D","ORC4L","SCHIP1","NSL1","CRBN","CRYZL1","ACYP2","MGST3","PEX19","MDH1","ATP5L","TSNAX","MAT2B","YPEL5","TCEAL1","CALM2","ATP5F1","COX5B","PEX11B","IMPA1","TTC1","GABARAPL2","NDUFS3","FBXO3","CCDC121","CRYL1","SNX11","GABRB2","SERPINI1","KCNK1","SNCG","CPNE6","KCNJ3","GRM1","VIP","HPCAL4","HPCA","CRYM","CCK","GPR22","CHN1","CA4","ADD3","CAMK2G","NTSR2","AGXT2L1","EDG1","MYBPC1","PPP1R1A","FEZF2","LOC201229","SLCO1A2","DHRS9","FLJ22655","THTPA","SLC30A10","ANXA3","FXYD1","PARP8","MAGEH1","SLC16A7","PGBD5","TPM3","CDC42","PSCD1","ENPP4","MMD","FAM49B","ARRB1","ROGDI","GRM3","SGK3","PLCL1","BEST1","KIAA1598","ENPP2","EDIL3","SLC31A2","EVI2A","TMEM144","CLCA4","MBP","UGT8","BCAS1","SH3GL3","REPS2","BASP1","DYNC1I1","SH3GL2","FUT9","ANKS1B","CDR1","ATRNL1","GNAI1","AGTPBP1","EPB41L3","FHIT","NANOS1","TCEAL2","PPFIA2","ANXA7","UROS","PPA1","SAR1A","CUTC","MSRB2","HPRT1","ACSL4","POPDC3","MGC72080","SEPP1","MORF4L2")

ltpm.gbm.all<-log2(gbm_tpm+1)

vk.cl.df<-ltpm.gbm.all[(rownames(ltpm.gbm.all) %in% vk.cl),]
vk.mes.df<-ltpm.gbm.all[(rownames(ltpm.gbm.all) %in% vk.mes),]
vk.PN.df<-ltpm.gbm.all[(rownames(ltpm.gbm.all) %in% vk.PN),]
vk.NL.df<-ltpm.gbm.all[(rownames(ltpm.gbm.all) %in% vk.NL),]

col=colorpanel(100, "yellow", "orange","dark red")

pheatmap(ltpm.gbm.all[rownames(vk.cl.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)

library(matrixStats)
vk.mes.df.mat<-as.matrix(vk.mes.df)
vk.mes.df.mat.sd<-transform(vk.mes.df.mat, SD=rowSds(vk.mes.df.mat, na.rm=TRUE))
vk.mes.df.mat.sd.clean <- vk.mes.df.mat.sd[vk.mes.df.mat.sd[,38]>0,]
vk.mes.df.hm<-vk.mes.df.mat.sd.clean[,1:37] 

pheatmap(ltpm.gbm.all[rownames(vk.mes.df.hm),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)
pheatmap(ltpm.gbm.all[rownames(vk.PN.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)
pheatmap(ltpm.gbm.all[rownames(vk.NL.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)

#### Trying to see which DEGs if they are in one of the signatures
#write.table(degs.PT.ST.final,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.degs.PTvsST.mf.txt",sep="\t",row.names=T)
degs.PT.ST.df <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/deseq2.degs.PTvsST.mf.txt",header=T,row.names=1)

ltpm.gbm.all.exp <- ltpm.gbm.all[keep,] ### To find hypergeomtric association just find how many genes are expressed in the system
dim(ltpm.gbm.all.exp) ## 22507 genes

degs.vk.cl.df<-degs.PT.ST.df[(rownames(degs.PT.ST.df) %in% vk.cl),] # 16 degs #pvalue p < 0.165 
degs.vk.mes.df<-degs.PT.ST.df[(rownames(degs.PT.ST.df) %in% vk.mes),] # 49 degs #p < 2.400e-12 
degs.vk.PN.df<-degs.PT.ST.df[(rownames(degs.PT.ST.df) %in% vk.PN),] # 14 degs # p < 0.454 
degs.vk.NL.df<-degs.PT.ST.df[(rownames(degs.PT.ST.df) %in% vk.NL),] #  9 degs # p < 0.485 

### Two ord plot for the enriched signatures another way of representing the verhaak signautures in PT vs REC
library(plotrix)
a=c(49,16,14,9)
b=c(11.62,0.78,0.34,0.32)
genes=c("MES","CL","PN","NL")
genesnum<-1:4
twoord.plot(genesnum,a, genesnum,b,type=c("bar","l"),ylab="Number of genes enriched per signature",rylab="Log10(P.Value)",xlab="Verhaak Signatures",lcol=17,rcol=3,do.first="plot_bg()", xticklab= genes,lpch=1,halfwidth=0.1,axislab.cex=1)



col=colorpanel(100, "black","white","dark red")
pheatmap(ltpm.gbm.all[rownames(degs.vk.cl.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)
pheatmap(ltpm.gbm.all[rownames(degs.vk.mes.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)
pheatmap(ltpm.gbm.all[rownames(degs.vk.PN.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)
pheatmap(ltpm.gbm.all[rownames(degs.vk.NL.df),], scale="row",annotation=batch.gbm[,1:5], fontsize=5.5,clustering_distance_cols="correlation",show_rownames = T,treeheight_col=40,color = col,show_colnames = F)

## combined heatmap of all signature in DEGs PT vs ST with annotation
verhaak.genes.degs.annot <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/DEGs.PTvsST.annot.txt",header=T,row.names=1)
degs.verhaak<-c("DLC1","ERCC2","JAG1","GAS1","GLI2","ZFHX4","TBX2","EGFR","MCC","SLC4A4","SHOX2","RGS6","PMP22","CDH2","EYA2","MEIS1","TES","CD4","COL1A1","THBS1","THBD","COL1A2","MAPK13","C5AR1","DAB2","MGAT1","ARHGAP29","MS4A4A","PROCR","STAT6","TGFBI","SEC24D","SH2B3","FHOD1","STXBP2","TCIRG1","SERPINE1","SLC16A3","LTBP2","LOX","TRIM38","SP100","LCP2","CDCP1","MYH9","S100A4","ITGA5","DSE","TEC","TNFRSF11A","MAFB","RAB11FIP1","PLAUR","TRPM2","PTGER4","COL5A1","FES","PTPRC","CD14","NOD2","VDR","FHL2","ITGA4","RAC2","MSR1","PLCB4","PCDH11X","MTSS1","KIF21B","CA10","RAB33A","SLC1A1","EPHB1","NRXN1","NR0B1","C1QL1","REEP1","NLGN3","GADD45G","ARRB1","ACSL4","CALM2","SLC30A10","SNCG","PPFIA2","PLCL1","CASQ1","FUT9")

ann_row <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/DEGs.PTvsST.annot.txt",header=T,row.names=1)

degs.vk.all.sig.df<-degs.PT.ST.df[(rownames(degs.PT.ST.df) %in% degs.verhaak),]

batch.gbm.new<-batch.gbm[,c("batch","cellTYPE","tumortypes","patient")]
pheatmap(ltpm.gbm.all[rownames(ann_row),],annotation_col=batch.gbm.new,  annotation_row=ann_row,fontsize=5.5,show_rownames = T,show_colnames = F,cluster_rows=F,cluster_cols=T,scale="row",color = col,border_color=NA)

### verhakk on all genes expressed
library(matrixStats)
verhak.PTvsST.exp.mat<-as.matrix(verhak.PTvsST.exp)
verhak.PTvsST.exp.sd<-transform(verhak.PTvsST.exp.mat, SD=rowSds(verhak.PTvsST.exp.mat, na.rm=TRUE))
verhak.PTvsST.exp.sd.clean <- verhak.PTvsST.exp.sd[verhak.PTvsST.exp.sd[,38]>0,]
verhak.PTvsST.exp.hm<-verhak.PTvsST.exp.sd.clean[,1:37] 
dim(verhak.PTvsST.exp.hm)
#[1] 682  37
verhak.PTvsST.exp.hm.clean<-na.omit(verhak.PTvsST.exp.hm)
dim(verhak.PTvsST.exp.hm.clean)
#[1] 632  37
pheatmap(verhak.PTvsST.exp.hm.clean,annotation_col=batch.gbm.new,  annotation_row=ann_row,fontsize=5.5,show_rownames = T,show_colnames = F,cluster_rows=F,cluster_cols=T,scale="row",color = col,border_color=NA)
dim(verhak.PTvsST.exp.hm.clean)
[1] 632  37

#### GO BP for the molecular subtypes 

## PT vs REC
### MES
up.in.REC.overlap.MES <- c("TNFRSF11A","NOD2","C5AR1","MAFB")
#keep <- rowSums(ovaca.tpm)>0
#ovaca.tpm.exp <- ovaca.tpm[keep,]
bg <- (rownames(ltpm.gbm.all.exp))
go <- go.enrichment(bg,up.in.REC.overlap.MES)
goTreemap(go$BP,removeParents = F) ## No terms
#goTreemap(go$MF,removeParents = F)

## NL
up.in.REC.overlap.NL <- c("FUT9","PLCL1","PPFIA2")
bg <- (rownames(ltpm.gbm.all.exp))
go <- go.enrichment(bg,up.in.REC.overlap.NL)
goTreemap(go$BP,removeParents = F) ## No terms

## For down in REC
#MES
down.in.REC.overlap.MES <- c("CD4","TES","THBD","COL1A1","MS4A4A","THBS1","COL1A2","STAT6","DAB2","TRPM2","TGFBI","LCP2","ARHGAP29","MAPK13","STXBP2","LOX","SLC16A3","CDCP1","SERPINE1","TEC","PTPRC","S100A4","TCIRG1","LTBP2","PTGER4","CD14","RAC2","PROCR","FHOD1","TRIM38","SEC24D","ITGA5","FES","COL5A1","SH2B3","MGAT1","DSE","VDR","ITGA4","RAB11FIP1","MSR1","PLAUR","MYH9","FHL2","SP100")
bg <- (rownames(ltpm.gbm.all.exp))
go <- go.enrichment(bg,down.in.REC.overlap.MES)## lot of terms need to try with other GO tools
goTreemap(go$BP,removeParents = F)

write.table(go$BP,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/GO.BP.down.in.REC.overlap.MES.txt",sep="\t",row.names=T)

write.table(go$MF,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/GO.MF.down.in.REC.overlap.MES.txt",sep="\t",row.names=T)
#goTreemap(go$MF,removeParents = F)

## PN
down.in.REC.overlap.PN <- c("PLCB4","KIF21B","RAB33A","MTSS1","SLC1A1")
bg <- (rownames(ltpm.gbm.all.exp))
go <- go.enrichment(bg,down.in.REC.overlap.PN) ## No terms
goTreemap(go$BP,removeParents = F)

## CL
down.in.REC.overlap.CL <- c("DLC1","JAG1","EGFR","ERCC2","GLI2","TBX2")
bg <- (rownames(ltpm.gbm.all.exp))
go <- go.enrichment(bg,down.in.REC.overlap.CL)
goTreemap(go$BP,removeParents = F)


write.table(ltpm.gbm.all.exp,"/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs.analysis.aug.2016/DEGs_sept.2016/Verhaak/GO_GTscripts/ltpm.gbm.all.bg.txt",sep="\t",row.names=T)