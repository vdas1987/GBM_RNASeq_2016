"udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z"


gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
dim(gbm_tpm)
keep <- rowSums(gbm_tpm)>0
gbm_tpm <- gbm_tpm[keep,]
dim(gbm_tpm)
colnames(gbm_tpm)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")

### pca plot on the gbm all samples and plot PC1 and PC2 the difference 
pca= prcomp( log2(gbm_tpm+1) , center=T, scale=T)
plot(pca$rotation[,1],pca$rotation[,2], xlab = "PC1", ylab = "PC2")
points(pca$rotation[1:27,], col = "red")
points(pca$rotation[28:37,], col = "green")
text(pca$rotation[,1],pca$rotation[,2], row.names(pca$rotation), cex=0.5, pos=4)

##spearman correlation of the gbm tpm for all samples
ltpm=log2(gbm_tpm+1)
mcor=cor(ltpm,method="spearman")
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,,cellwidth =10, cellheight =10,cluster_cols=T,clustering_distance_rows="correlation",clustering_distance_cols="correlation")


### using the batch correction from edgeR on the logCPM derived from gbm.counts. matrix
library(limma)
library(edgeR)
library(sva)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
#[1] 24735    37
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
#[1] 22507    37
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")

gbm.counts.round<-round(gbm.counts)
dge <- DGEList(counts= gbm.counts.round)
dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)
batch <- read.table("~/Desktop/batch.txt",header=T,row.names=1)
### plot the leading logFC samples with mds plot before removing the batch
plotMDS(logCPM,dim.plot=c(1,2),col=as.numeric(batch$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
###correcting for batch effect removing the known batch of 2 sequencing facility (udine,ieo)
logCPM.corrected <- removeBatchEffect(logCPM,batch=batch$batch)
### plot the leading logFC samples with mds plot after removing the batch
plotMDS(logCPM.corrected,dim.plot=c(1,2),col=as.numeric(batch$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
### creating the correlation with spearman on the logCPM before batch
mcor=cor(logCPM,method="spearman")
### plotting the above non correct logCPM correlated data
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,,cellwidth =10, cellheight =10,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean")
### creating the correlation with spearman on the logCPM after batch removal
mcor2=cor(logCPM.corrected,method="spearman")
#### plotting the correlation of above with correct logCPM after removing the batch effect
pheatmap(mcor2,fontsize_row=4.8,fontsize_col=4.8,,cellwidth =10, cellheight =10,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean")


### Using combat on gbm_tpm does not work
#log.gbm.tpm=log(gbm_tpm+1)
#mm <-model.matrix(~batch$cellTYPE)
#log.gbm.tpm.corrected.combat <- sva::ComBat(log.gbm.tpm, batch=as.factor(batch$batch), mod = mm)
#plotMDS(logCPM.corrected.combat,dim.plot=c(1,2),col=as.numeric(batch$batch), cex=0.6,xlab="PC1",ylab="PC2")


### Using combat on logCPM
#gbm.counts.round<-round(gbm.counts)
#dge <- DGEList(counts= gbm.counts.round)
#dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)
batch <- read.table("~/Desktop/batch.txt",header=T,row.names=1)
mm <-model.matrix(~batch$cellTYPE)
logCPM.corrected.combat <- sva::ComBat(logCPM, batch=as.factor(batch$batch), mod = mm)
plotMDS(logCPM.corrected.combat,dim.plot=c(1,2),col=as.numeric(batch$batch), cex=0.6,xlab="Dim1",ylab="Dim2")
mcor=cor(logCPM.corrected.combat,method="spearman")
pheatmap(mcor,fontsize_row=4.8,fontsize_col=4.8,cellwidth =10, cellheight =10,cluster_cols=T,clustering_distance_rows="euclidean",clustering_distance_cols="euclidean")


### Using SVA
design <- model.matrix(~batch$batch+batch$cellTYPE)
design0 <- model.matrix(~batch$batch)
# one design0 with all but the one you one to test
# work on lognorm(readcounts)

### lnj is a log-norm-transformed read-counts table
n.sv <- num.sv(logCPM,design,method="leek")
## how many surrogate variables did it find? n.sv
sv <- sva(as.matrix(logCPM),design,design0,n.sv)
#with a small help from PL you can correct for the right scaling factor and plot the PCA of lognorm(counts)
# Do you still see worrisome patterns?
svaBatchCor <- function(dat, mmi, n.sv=NULL){
    dat <- as.matrix(dat)
    Y <- t(dat)
    library(sva)
    if(is.null(n.sv))   n.sv <- num.sv(dat,mmi,method="leek")
    o <- sva(dat,mmi,n.sv=n.sv)
    W <- o$sv
    alpha <- solve(t(W) %*% W) %*% t(W) %*% Y
    o$corrected <- t(Y - W %*% alpha)
    return(o)
}
############ corrected(lognorm(counts))
logCPM.svaseq <- svaBatchCor(logCPM,design,n.sv=n.sv)
###########
##plot MDS on batch corrected with SVA
plotMDS(logCPM.svaseq$corrected,dim.plot=c(1,2),col=as.numeric(batch$batch), cex=0.6,xlab="Dim1",ylab="Dim2")


plotPCA(lnj.corr$corrected)
GTscripts::plotPCA(lnj.corr$corrected) ###maybe
### now you can create a design and use also the sv!
design <- model.matrix(~gender+batch+condition+sv$sv)

### Test run for primary vs secondary diff expression finding of GBM 
batch <- read.table("~/Desktop/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
gbm.counts.round<-round(gbm.counts)
dge <- DGEList(counts= gbm.counts.round)
dge <- calcNormFactors(dge)
logCPM <- cpm(dge,log=TRUE,prior.count=5)
design <- model.matrix(~batch$batch+batch$cellTYPE+batch$tumortypes)
design0 <- model.matrix(~batch$batch+batch$cellTYPE)
# one design0 with all but the one you one to test
# work on lognorm(readcounts)

### lnj is a log-norm-transformed read-counts table
n.sv <- num.sv(logCPM,design,method="leek")
## how many surrogate variables did it find? n.sv
sv <- sva(as.matrix(logCPM),design,design0,n.sv)
#with a small help from PL you can correct for the right scaling factor and plot the PCA of lognorm(counts)
# Do you still see worrisome patterns?
svaBatchCor <- function(dat, mmi, n.sv=NULL){
    dat <- as.matrix(dat)
    Y <- t(dat)
    library(sva)
    if(is.null(n.sv))   n.sv <- num.sv(dat,mmi,method="leek")
    o <- sva(dat,mmi,n.sv=n.sv)
    W <- o$sv
    alpha <- solve(t(W) %*% W) %*% t(W) %*% Y
    o$corrected <- t(Y - W %*% alpha)
    return(o)
}
############ corrected(lognorm(counts))
lnj.corr <- svaBatchCor(logCPM,design,n.sv=n.sv)

### does not work that well, so may be use combat and then limma for differential expression

### with limma-voom for primary vs secondary degs finding and correcting for batch of facility wit combat and then plotting the degs for volcano and heatmap(did not use svaseq since there were no other confounding variables , in short only the facilty difference was the driving batch which can either be removed by removebatch() from edgeR or combat for known batches and then and used limma voom for differential expression
library(edgeR)
library(sva)
library(limma)
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
gbm.counts.round<-round(gbm.counts)
dge <- DGEList(counts= gbm.counts.round)
design <- model.matrix(~batch$batch+batch$tumortypes)
rownames(design) <- colnames(dge)
dge$samples$lib.size <- colSums(dge$counts)
dge <- calcNormFactors(dge) ## already normalizing here with TMM once and then feed to voom function which usually log tranform the counts this can be fed to combat and then that to lmFIT
## combat-limma voom

v <- voom(dge, design, plot=TRUE)
combat_edata <- ComBat(dat=v$E, batch=batch$batch, mod=design[,1], par.prior=TRUE, prior.plots=TRUE)
fit <- lmFit(combat_edata, design)
fit <- eBayes(fit)
top <- topTable(fit,coef=3,number=Inf,sort.by="P")
sum(top$adj.P.Val<0.05)
degs<-topTable(fit, adjust.method="BH", coef=3,number=Inf,sort.by="P")
ind<-which(degs$adj.P.Val<0.05)
degs2<-degs[ind,]
dim(degs2)
###volcano plot for degs with adj p value less than 0.01
# Make a basic volcano plot
with(degs, plot(logFC, -log10(adj.P.Val), pch=20, main="Volcano plot",))
# Add colored points: red if adj.P.Val<0.01, orange of logFC>1.5, green if both)
with(subset(degs, adj.P.Val <0.01 ), points(logFC, -log10(adj.P.Val), pch=20, col="red"))
with(subset(degs, abs(logFC)>1.5), points(logFC, -log10(adj.P.Val), pch=20, col="orange"))
with(subset(degs, adj.P.Val <0.01 & abs(logFC)>1.5), points(logFC, -log10(adj.P.Val), pch=20, col="green")) ##degs with logFC 1.5 on either side and adj.pval 0.01
ind<-which(degs$adj.P.Val<0.01 & abs(top$logFC)>1.5)
degs_0.01_1.5<-degs[ind,]
write.table(degs_0.01_1.5,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/degs_primary_vs_secondary_gbm_logfc1.5_adpval_0.01.txt",sep="\t",row.names=T)
## heatmap using gtscripts of Pierre
library(GTscripts)
gbm_tpm <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.tpm",header=T,row.names=1)
colnames(gbm_tpm) <- rownames(batch)
### pheatmap with GTscripts
rgheatmap(gbm_tpm[rownames(degs_0.01_1.5),], scale="row",annotation=batch, fontsize=5.5,clustering_distance_cols="euclidean",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/hm-degs_pri_vs_rec_gbm.png")

### have to redo the plots removing the genes that are for TMZ
### plotting DEGs removing the TMZ resistant genes
barplot(as.matrix(gbm_tpm[grepl("GLUT-3", rownames(gbm_tpm)), ]),cex.names=0.5,las=2)

tmz_genes<-c("GLUT1","GLUT2","GLUT3","GLUT10","AKR1C1", "AKR1C2","AKR1C3","SCRG1","IGSF4D","SLC2A10","SPP1","SLC2A5","SCG2") #Benjamin le calve eta. al neoplasia ,2010
tmz_genes<-c("MDR1","PTCH1") #Jessian L Munoz et al, Cancer Cell & Microenvironment 2015
tmz_genes<-c("MGMT","APNG","BER","NAMPT","BCRP1","LIF","TNFAIP3","MSH6","MSH2") #Lee SY et all , Genes and Diseases 2016
## out of the above only below 3 genes are DEGs so remove the degs from the list having these 3 genes and make the plot
tmz_clean<-as.matrix(degs_0.01_1.5[!grepl("SPP1|SCG2|LIF", rownames(degs_0.01_1.5)), ])
tmz_clean <-data.frame(tmz_clean)
### replot
rgheatmap(gbm_tpm[rownames(tmz_clean),], scale="row",annotation=batch, fontsize=5.5,clustering_distance_cols="euclidean",show_rownames = F,treeheight_col=40,filename="~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/hm-degs_pri_vs_rec_gbm_clean.png")
#cleaning for volcano plot
degs_clean<-as.matrix(degs[!grepl("SPP1|SCG2|LIF", rownames(degs)), ])
degs_clean <-data.frame(degs_clean)
###volcano plot for degs with adj p value less than 0.01
# Make a basic volcano plot
with(degs_clean, plot(logFC, -log10(adj.P.Val), pch=20, main="Volcano plot",))
# Add colored points: red if adj.P.Val<0.01, orange of logFC>1.5, green if both)
with(subset(degs_clean, adj.P.Val <0.01 ), points(logFC, -log10(adj.P.Val), pch=20, col="blue"))
with(subset(degs_clean, abs(logFC)>1), points(logFC, -log10(adj.P.Val), pch=20, col="orange"))
with(subset(degs_clean, adj.P.Val <0.01 & abs(logFC)>1.5), points(logFC, -log10(adj.P.Val), pch=20, col="grey60"))

### so this primary vs secondary is fine and works well will put it in a very cleaner way in a new script and rerun again 11-08-2016

#### Z vs PGRT for samples. # 132z, 118z, 141z ,183z, 127z, 168z vs 132p1, 141p1, 183p2,p3, 127p1,168p3,p4
batch <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/scripts/batch.txt",header=T,row.names=1)
gbm.counts <- read.table("~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/salmon.genes.counts",header=T,row.names=1)
dim(gbm.counts)
colnames(gbm.counts)<-c("udine.118p","udine.118rp3","udine.118rz","udine.118z","udine.132p1","udine.132p2","udine.132p3","udine.132rp1","udine.132rp3","udine.132rp4","udine.132rz1","udine.132rz2","udine.132z","udine.141p1","udine.141p2","udine.141p3","udine.141p4","udine.141z","udine.183p1","udine.183p2","udine.183p3","udine.183z","udine.91p","udine.91rp1","udine.91rp3","udine.91rp4","udine.91rz","ieo.127P1","ieo.127P2","ieo.127P3","ieo.127P4","ieo.127Z","ieo.168P1","ieo.168P2","ieo.168P3","ieo.168P4","ieo.168Z")
keep <- rowSums(gbm.counts)>0
gbm.counts <- gbm.counts[keep,]
gbm.counts.round<-round(gbm.counts)
gbm.z.pgrt<-gbm.counts.round[,c("udine.118z","udine.132z","udine.141z","udine.183z","ieo.127Z","ieo.168Z","udine.132p1","udine.141p1","udine.183p2", "udine.183p3","ieo.127P1","ieo.168P3", "ieo.168P4")]
dim(gbm.z.pgrt)
keep <- rowSums(gbm.z.pgrt)>0
gbm.z.pgrt <- gbm.z.pgrt[keep,]
batch.z.pgrt<-batch[colnames(gbm.z.pgrt),]
write.table(batch.z.pgrt,"~/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.z.pgrt.txt",sep="\t",row.names=T)
batch.z.pgrt <- read.table("/Users/vdas/Documents/Test_lab_projects/Bonn_analysis_07012014/Bonn_analysis_2016/RNA-Seq/quality_control_bonn/batch.z.pgrt.txt",header=T,row.names=1)
###DEGs with limma voom

dge <- DGEList(counts= gbm.z.pgrt)
design <- model.matrix(~batch.z.pgrt$batch+batch.z.pgrt$cellTYPE)
rownames(design) <- colnames(dge)
dge$samples$lib.size <- colSums(dge$counts)
dge <- calcNormFactors(dge)
## combat-limma voom
v <- voom(dge, design, plot=TRUE)
combat_edata <- ComBat(dat=v$E, batch=batch.z.pgrt$batch, mod=design[,1], par.prior=TRUE, prior.plots=TRUE)
fit <- lmFit(combat_edata, design1)
fit <- eBayes(fit)
top <- topTable(fit,coef=3,number=Inf,sort.by="P")
sum(top$adj.P.Val<0.05)
degs<-topTable(fit, adjust.method="BH", coef=3,number=Inf,sort.by="P")
### limma does not give any DEGs with with z v pgrt so will be trying deseq2 here 